package it.unipd.dei.ims.FeaturesExtraction.terrier;

import java.io.IOException;

import org.terrier.matching.ResultSet;
import org.terrier.matching.models.WeightingModelLibrary;
import org.terrier.querying.Manager;
import org.terrier.querying.SearchRequest;
import org.terrier.structures.DocumentIndex;
import org.terrier.structures.Index;
import org.terrier.structures.Lexicon;

/** This class performs retrieval tasks
 * 
 * @author	Paolo Picello
 * @email	paolo.picello@studenti.unipd.it
 * 
 */
public class Retrieval {

	
	/**
	* Return the resultSet for a given query and a given weighting model
	* @param query The query.
	* @param queryingManager The manager.
	* @param srq SearchResult.
	* @param rs ResultSet
	* @param weightingModel The weighting model used.
	* @throws IOException if cannot find the term.
	* @return The resultSet
	*/
	public static ResultSet getResultSet(String query, Manager queryingManager, SearchRequest srq, ResultSet rs, String weightingModel) {

		srq = queryingManager.newSearchRequest("query", query);
		srq.addMatchingModel("Matching", weightingModel);
		queryingManager.runPreProcessing(srq);
		queryingManager.runMatching(srq);
		queryingManager.runPostProcessing(srq);
		queryingManager.runPostFilters(srq);
		
		rs = srq.getResultSet();
		return rs;	
	}
	
	/**
	 * Compute TF_IDF array for a particular document and field (because different fields have different termFreqs)
	 * TF = n. term's occurences termine in the doc/doc. dimension
	 * IDF = log(n. doc in the collection/n. doc that contains term ti) 
	 * TF_IDF = TF * IDF
	 * @param index
	 * @param docid
	 * @param termFreqs
	 * @param terms
	 * @param doi
	 * @param lex
	 * @return
	 */
	public static double[] getTF_IDF(Index index, int docid, double[] termFreqs, String[] terms, DocumentIndex doi, Lexicon lex, int field) {
		
		int docLength = 0;
		int totalDocs;
		double tf_idf[] = new double[termFreqs.length];
		int[] docWithTerm = new int[termFreqs.length];

		
		try {
			if(field==-1)
				docLength = Utility.getDocumentLength(docid, doi);
			else 
				docLength = Utility.getDocumentFieldLengthbyId(field, docid, doi);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		totalDocs = index.getCollectionStatistics().getNumberOfDocuments();

		
		for(int m=0; m<terms.length; m++) {
			docWithTerm[m] = Utility.howManyDocumentsWithTerm(terms[m], lex);
		}
		
		// per ogni termine nella collezione calcolo il TF_IDF e prendo il massimo
		for(int i = 0; i < termFreqs.length; i++) {
			
			double tf = termFreqs[i] / docLength;
			double idf = 0;
			if(docWithTerm[i]!=0) { //questo xk altrimenti succede capiti divisione x 0
				idf = WeightingModelLibrary.log ( totalDocs/ docWithTerm[i]);
			}
			else {
				idf = 0;
			}
			
			tf_idf[i] = tf * idf;
		}
		
		return tf_idf;
	}
	
}
