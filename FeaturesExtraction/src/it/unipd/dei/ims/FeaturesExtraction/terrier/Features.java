package it.unipd.dei.ims.FeaturesExtraction.terrier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import org.apache.commons.lang.ArrayUtils;


/** This class models a single Feature and controls the letor features 
 * 
 * @author	Paolo Picello 
 * @email	paolo.picello@studenti.unipd.it
 *  
 */
public class Features {

	String class_name = "";
	String data_type = "";
	int field_index;
	
	static int num_integer_features;
	static int num_double_features;
	static int num_features;
	static String[] field_names;
	
	static List<Features> features = new ArrayList<Features>();

	public Features (String classname, String datatype, int fieldIndex) {
		this.class_name = classname;
		this.data_type = datatype;
		this.field_index = fieldIndex;
	}
	
	public static void readFeatures (Properties prop) throws IOException {
		String featurestext = prop.getProperty("features");
		
		// Read features from properties
		num_features = 0;
		Scanner scanner = new Scanner(featurestext);
		while(scanner.hasNext()) {
			scanner.next();
			num_features++;
		}
		scanner.close();
		String[] featuresNames = new String[num_features];
		scanner = new Scanner(featurestext);
		int feat_cnt = 0;
		while(scanner.hasNext()) {
			String feature = scanner.next();
			featuresNames[feat_cnt] = feature;
			feat_cnt++;
		}
		scanner.close();
		// in featuresNames[] we have the features as we found in the properties
		
	
		InputStream input = null;

		input = new FileInputStream(Execution.terrier_properties); 
		prop.load(input); // load a properties file	
		
		if(prop.containsKey("FieldTags.process")) { // there are fields considered
			String fields = prop.getProperty("FieldTags.process");
			scanner = new Scanner(fields);
			String word = scanner.next();
			field_names = word.split(","); 
			
			// now we have something like ...
			// field_names[0] = TEXT
			// field_names[1] = HEADLINE
		}
		
		
		/** for each features in properties */
		for(int i = 0; i < featuresNames.length; i++) {

			String classname = "";
			String datatype = "";
			int fieldIndex = -1;
			
			// CASO 1: feature contains "-" character
			if(featuresNames[i].contains("-")) { //PER FIELD (c'è il "-"..da li in poi deve esserci nome field però sennò pacco)
				int pos = featuresNames[i].indexOf("-"); // position in which there is -
				String nome_field = featuresNames[i].substring(pos+1, featuresNames[i].length()); //field as String
				
				if(ArrayUtils.contains(field_names, nome_field)) { // already found field
					int index = ArrayUtils.indexOf(field_names, nome_field); // the index in the array in which there is this entry
					fieldIndex = index;
					classname = featuresNames[i].substring(0, pos); // get the name of the feature

					datatype = prop.getProperty(classname + ".datatype");
					classname = prop.getProperty(classname + ".class");
					
					if(datatype.equals("java.lang.Integer")) num_integer_features++;
					else num_double_features++;
				}
				else {
					Execution.logger.fatal("Not defined field " + nome_field + ".. Check FieldTags.process in terrrier.properties ");
					System.exit(0);
				}
				
				
			}
			
			else if(featuresNames[i].startsWith("terrier.")) { // TERRIER FEATURES
					classname = featuresNames[i];
					datatype = "java.lang.Double";
					fieldIndex = -1;
					num_double_features++;
			}
			
			// CASO 2: whole document feature
			else {
				datatype = prop.getProperty(featuresNames[i] + ".datatype");
				classname = prop.getProperty(featuresNames[i] + ".class");
				fieldIndex = -1; // -1 tells this is a whole document feature
				if(datatype.equals("java.lang.Integer")) num_integer_features++;
				else num_double_features++;
			}
			
			Features newFeature = new Features(classname, datatype, fieldIndex);
			features.add(newFeature);
			
		}
	}
	
	/**
	 * Write feature.proprties in the same folder as table_path..
	 * This file will be useful in Picello2 to know how features are written
	 * in memory.. It indicates the order and also the type of each used feature.
	 * This file have to be made read-only calling file.setReadOnly() to 
	 * prevent unwanted modifications!
	 * 
	 * Info: Run "chmod 775 + full/path" to make the file writable
	 * 
	 * @param prop Picello1 properties
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	public static void createFeatureProperties(Properties prop) throws IOException {
		
		if(new File(Execution.table_full_path + "/features.properties").exists()) {
			final Process p = Runtime.getRuntime().exec("chmod 775 " + Execution.table_full_path + "/features.properties");

			new Thread(new Runnable() {
			    public void run() {
			     BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			     String line = null; 

			     try {
			        while ((line = input.readLine()) != null)
			            System.out.println(line);
			     } catch (IOException e) {
			            e.printStackTrace();
			     }
			    }
			}).start();

			try {
				p.waitFor();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		
		File read_only_file = new File(Execution.table_path +  "/features.properties");
		PrintWriter pw = new PrintWriter(read_only_file);

		pw.write("#This file is READ-ONLY to prevent unwanted changes\n");
		pw.write("features=");
		

		InputStream input = null;
		input = new FileInputStream(Execution.terrier_properties); 
		prop.load(input); // load a properties file	
		
		if(prop.containsKey("FieldTags.process")) { // there are fields considered
			String fields = prop.getProperty("FieldTags.process");
			Scanner scanner = new Scanner(fields);
			String word = scanner.next();
			field_names = word.split(","); 
			
			System.out.println("PROVA: " + field_names[0] + "\n");
			System.out.println("PROVA: " + field_names[1] + "\n");
			System.out.println("PROVA: " + field_names[2] + "\n");
			
			// now we have something like ...
			// field_names[0] = TEXT
			// field_names[1] = HEADLINE
		}
		
		// Write features information... The order is IMPORTANT here,
		// it specifies how to access the tables!!!
		for(int i = 0; i<Features.features.size(); i++) {
						
			Features tmp = Features.features.get(i);
			
			System.out.println("FIELD: " + tmp.field_index);

			String name_tmp = tmp.class_name;
			if(!name_tmp.startsWith("terrier.")) {
				name_tmp = name_tmp.substring("it.unipd.dei.ims.FeaturesExtraction.".length(), name_tmp.length());
				if(tmp.field_index==-1)
					pw.write(name_tmp + " \\" + "\n");
				else {
					pw.write(name_tmp + "-" + field_names[tmp.field_index] + " \\" + "\n");
				}
				
			}
			else { 
					pw.write(name_tmp + " \\" + "\n");
			}
			
		}
		
		pw.write("\n");
		
		// keep track of the already written features deinitions
		// beacuse we could have more thn one feature, defined on different fields
		List<String> list_feat_written = new ArrayList<String>(); 
		
		for( int i =0; i < Features.features.size(); i++) {
			Features tmp = Features.features.get(i);
			String name_tmp = tmp.class_name;
			if(!name_tmp.startsWith("terrier.") && !list_feat_written.contains(name_tmp)) {
				list_feat_written.add(name_tmp);
				name_tmp = name_tmp.substring("it.unipd.dei.ims.FeaturesExtraction.".length(), name_tmp.length());
			
				pw.write(name_tmp + ".class=" + prop.getProperty(name_tmp + ".class") + "\n");
				pw.write(name_tmp + ".datatype=" + prop.getProperty(name_tmp + ".datatype") + "\n");
				pw.write("\n");
			}
		}
		
		pw.write("\n");
		pw.close();

	}
}
