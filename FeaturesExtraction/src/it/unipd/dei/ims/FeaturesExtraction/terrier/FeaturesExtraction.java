package it.unipd.dei.ims.FeaturesExtraction.terrier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import org.terrier.matching.ResultSet;
import org.terrier.querying.Manager;
import org.terrier.querying.SearchRequest;
import org.terrier.structures.BitIndexPointer;
import org.terrier.structures.DocumentIndex;
import org.terrier.structures.Index;
import org.terrier.structures.Lexicon;
import org.terrier.structures.MetaIndex;
import org.terrier.structures.PostingIndex;

import it.unipd.dei.ims.FeaturesExtraction.features.Scoring;


/**
 * This class extracts LETOR features from index
 * 
 * @author Paolo Picello
 * @email paolo.picello@studenti.unipd.it
 *
 */
public class FeaturesExtraction {

	/**
	 * This method computes Letor features and write them to memory. It writes
	 * also a pointerMemory containing the pointers to the position in memory in
	 * which the entry for this document starts (byte offset). At the end of
	 * this method we will have resources/tables with a file for each query
	 * containing the features (bytes) and resources/memoryPointers containing
	 * an hashmap indicating for each document the position (byte) at which we
	 * can found it in the respective file.
	 * 
	 * @param index The index.
	 * @param runNames The array containing names of run file
	 * @param runsFolder The folder in which there are the run files.
	 * @param stemmed Bollean indicating if we are using a Stemmer or not.
	 * @throws NoSuchElementException
	 * @throws IOException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	public static void computeLetor(Index index, String[] runNames)
			throws NoSuchElementException, IOException, NoSuchMethodException, SecurityException,
			ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {

		/** Index structures */
		Lexicon<String> lex = null;
		DocumentIndex doi = null;
		PostingIndex<?> pi;
		MetaIndex meta;
		
		Manager queryingManager = new Manager(index);
		SearchRequest srq = null;
		ResultSet rs = null;

//		long TOTtimeTime = System.currentTimeMillis();

		
		int minQid = Integer.parseInt(Topics.topics.get(0).qid); // 351
		int maxQid = Integer.parseInt(Topics.topics.get(Topics.topics.size()-1).qid); // 450
		Execution.logger.debug("min: " + minQid + " max: " + maxQid);

		File tablesFolder = new File(Execution.table_full_path); 
		File pointersMemoryFolder = new File(Execution.pointers_path);
		
		/** Check is table & pointers folder are already been created.. if yes stop the execution
		 * 	bacause this is a dangerous situation.. you could throw away hours of work!!
		 *  If you want to overwrite you have to be sure!!
		 */
		if (tablesFolder.exists() || pointersMemoryFolder.exists()) {
			if(tablesFolder.exists())
				Execution.logger.fatal("Tables folder already exists. Please specify a new folder or empty the current one");
			if(pointersMemoryFolder.exists())
				Execution.logger.fatal("Pointers memory folder already exists. Please specify a new folder or empty the current one");
			System.exit(0);
		}
		else { // if folders not present create them
			if(!tablesFolder.exists())
				tablesFolder.mkdirs();
			if(!pointersMemoryFolder.exists())
				pointersMemoryFolder.mkdirs();
		}

		String tableName = Execution.table_path + "/table" + minQid + ".txt";

		/*---------------------------------------------------------
		 *  Load the index
		 *---------------------------------------------------------*/
		pi = index.getDirectIndex(); // returns the DirectIndex
		doi = index.getDocumentIndex(); // returns the DocumentIndex
		lex = index.getLexicon(); // returns the Lexicon
		meta = index.getMetaIndex(); // returns the MetaIndex
		Execution.logger.info("Finished to load the index.");

		int totaldocs = index.getCollectionStatistics().getNumberOfDocuments();
		String total_docs = "" + totaldocs;
		System.setProperty("matching.retrieved_set_size", total_docs); // set the retrieved set size

		
		/**
		 * Count how many features are field features and keep track of which
		 * are these fields. fieLdIndexes contains the fields for which we are
		 * interested to compute features.
		 */
		List<Integer> fieldIndexes = new ArrayList<Integer>();
		int count_terrier_features = 0;

		/*---------------------------------------------------------------------
		 *  Extract fields for which we are interested and terrier features number
		 *---------------------------------------------------------------------*/
		for (int k = 0; k < Features.features.size(); k++) {
			Features feature = Features.features.get(k);
			int field = feature.field_index;
			if (!fieldIndexes.contains(field) && field != -1) { // if not cotains it and different to -1
				fieldIndexes.add(field);
			}

			if (feature.class_name.startsWith("terrier."))
				count_terrier_features++; // count terrier features
		}

		
		/**
		 * hashMap for memory pointers, terrier scores, term_frequencies
		 * per field and tf_idf per field
		 * INITIALIZATION: we initialize the hashmap to the value (N / 0.75) + 1
		 * where N is the numebr of element i want to put (reasonably) in the hashmap,
		 * 0,75 is the default load factor (the hashmap will be resized when it reaches
		 * the 75% of its size 
		 * We initialize the size of the hashmap for pointers and terrier features as num_documents/10 (cira 50.000 per Tipster)
		 */
		
		double factor = ((totaldocs/10) / 0.75) +1; // (N/0.75)+1
		HashMap<String, Long> pointerMemory = new HashMap<String, Long>((int)factor); // memory pointers hashmap. Reset for every new qid (when we finish to process files in a folder for a particulaar qid)
		HashMap<Integer, double[]> terrierHashMap = new HashMap<Integer, double[]>((int)factor); // terrier' features hashmap. Reset for new qid. 													
		HashMap<Integer, double[]> fieldTermFreq = new HashMap<Integer, double[]>(fieldIndexes.size()); // TermFreq per field hashmap.	Reset for new qid. Initialize the size as the number of fields we are interested		
		HashMap<Integer, double[]> fieldTFIDF = new HashMap<Integer, double[]>(fieldIndexes.size()); // TF_IDF per field hashmap. Initialize the size as the number of fields we are interested		
																					

		int actualQID = minQid;
		boolean doc_non_present = false;
		
		/*---------------------------------------------------------
		 *  MAIN LOOP ( For every topic(qid) )
		 *---------------------------------------------------------*/
		while (actualQID <= maxQid) { 
			
//			long timeTime = System.currentTimeMillis();

			tableName = Execution.table_full_path + "/table" + actualQID + ".txt"; // name of the file in which our letors will be saved (as bytes)
			RandomAccessFile raf = new RandomAccessFile(tableName, "rw"); // Create a RAF to the file

			boolean firstForqid = true; // Useful to indicate if it is the first for this qid, we populate terrierHashMap only the first time for a new qid

			// ---------- Get topic and queryTerms
			int queryIndex = actualQID - minQid; // 351 is at index 0, 352 at index 1 etc..
			Topic topic = Topics.topics.get(queryIndex); // get Topic object
			String queryterms = topic.text; // query text (this will be our query terms)
			Execution.logger.debug("Query " + actualQID + " terms: " + queryterms);
					
			// Get number of terms in query
			Scanner scanner = new Scanner(queryterms);
			int query_cnt = 0;
			while(scanner.hasNext()) {
				scanner.next();
				query_cnt++;
			}	
			scanner.close();
			scanner = new Scanner(queryterms);
			// Fill array with query terms
			String[] queryTerms = new String[query_cnt];
			int tmp = 0;
			while(scanner.hasNext()) {
				String queryTerm = scanner.next();
				queryTerms[tmp] = queryTerm;
				tmp++;
			}	
			// ---------- End Get topic and queryTerms

			
			if(Execution.logger.isDebugEnabled()) {
				for ( int q=0; q<queryTerms.length; q++) {
					Execution.logger.debug("Term n." + q + " " + queryTerms[q]);
				}
			}
			
			/*---------------------------------------------------------
			 *  For each run file
			 *---------------------------------------------------------*/
			for (int u = 0; u < runNames.length; u++) { // for every run file (In runNames we have the names of the run file)
				
				Execution.logger.info("Calculating for run:" + runNames[u] + " num:" + u);
				Execution.logger.info("Actual qid: " + actualQID);
				String filename = Execution.runsFolder + "/" + runNames[u];
				BufferedReader run = new BufferedReader(new FileReader(filename));
				List<String> listDocno = Run.readRunByQueryID(run, actualQID); // Get the list of docno in that run with qid == actualQID
				run.close();


				/*---------------------------------------------------------
				 *  For each found DOCNO with that QID 
				 *---------------------------------------------------------*/
				for (int t = 0; t < listDocno.size(); t++) { // for each DOCNO with that queryID for this file
					doc_non_present = false;
					long startTime = 0;
					if(Execution.logger.isTraceEnabled()) // we don't want to get time info if we are in not in trace mode
						startTime = System.currentTimeMillis();
						//startTime = System.nanoTime(); 

					/**
					 * Firstly we verify that features for this docno are not
					 * already computed, which is possible.. If they are already
					 * computed (there is an entry in the hashmap with raf
					 * pointers) we don't have to do anything else because our
					 * features are already been computed for this docno/qid
					 * couple and they are in the raf position specified by the
					 * raf pointers hashmap (pointerMemory).
					 */
					long pointer;
					String docno = "" + listDocno.get(t); // get the docno

					Execution.logger.debug("Calculating for document " + docno);
					
					// ---------- Get docid 
					long docidTime = 0;
					if(Execution.logger.isTraceEnabled()) // we don't want to get time info if we are in not in trace mode
						docidTime = System.currentTimeMillis();
					
					int docid = Utility.getDocumentIDFromDOCNO(docno, doi, meta);
					
					if(Execution.logger.isTraceEnabled()) 
						Utility.getTime(docidTime, "get docid from docno");
					//----------------------
					
					// Verify we have this document in the index.. 
					try {
						pi.getPostings((BitIndexPointer) doi.getDocumentEntry(docid)); 
					} catch (NullPointerException e) {
						doc_non_present = true; // 
					}
					// ----------

					if (doc_non_present == true) {
						Execution.logger.warn("Document " + docno + " not found in the index");
						Execution.logger.warn("Ignoring this entry!");					}

					else { // Document present in the index ( => we can compute (if not already done) and write on disk )

						if(pointerMemory.containsKey(docno)) 
							pointer = pointerMemory.get(docno); // pointer is the position in memory (byte offset) in which the entry for this docno starts
						else pointer = -1; // not already computed features or this docno
					

						/*-----------------------------------------------------------------------
						 * Not already computed features for docid/qid 
						 *-----------------------------------------------------------------------*/
						if (pointer == -1) { // Not already computed features vector, we have to cmpute it, write it on disk and save the pointer in pointerMemory hashmap..

							// ---------- Get the relevance judgment
							int relJud = 0;
							String docKey = docno + "/" + actualQID;
							if(Pool.hashMap.containsKey(docKey))
								relJud = Pool.hashMap.get(docKey); // get the relevance judgment from the pool hashmap
							else relJud = 0;
							// ----------

							// --------- Save memory offset
							long posInMemoria = raf.getFilePointer(); // byte pointer for raf
							pointerMemory.put(docno, posInMemoria); // save the position in which the vector for this docno starts
							// ----------
							
							double[] termFreqs = new double[queryTerms.length]; // array of the frequencies of the terms in the document								
							double[] tf_idf = new double[queryTerms.length];
							// ------------

							/*-----------------------------------------------------------------------
							*  Terrier' features (e.g. BM25, TF_IDF, SingleFieldModel(BM25,3), ...)
							*-----------------------------------------------------------------------*/
							long startTime2 = 0;
							if(Execution.logger.isTraceEnabled()) // we don't want to keep time info if we are in not in trace mode
								 startTime2 = System.currentTimeMillis();
							if (firstForqid) { // Only if this is the first for this qid compute Terrier features and populate terrierHashMap
							
								String wm = "";
								int count = 0;
								for (int k = 0; k < Features.features.size(); k++) { // for each (Terrier) feature
									Features feature = Features.features.get(k);
									if (feature.class_name.startsWith("terrier.")) { // follow the syntax specified in properties file
										wm = feature.class_name.substring("terrier.".length(),feature.class_name.length());
										rs = Retrieval.getResultSet(queryterms, queryingManager, srq, rs, wm);
										Utility.populateHashMap(count, rs, terrierHashMap, count_terrier_features);
										count++;
									}
								}
								firstForqid = false; // in this way this will not be executed until next qid
							} // end terrier features computation
							/**
							 * Now we have terrierHashMap with terrier features
							 * fo every docno
							 */
							if(Execution.logger.isTraceEnabled()) 
								Utility.getTime(startTime2, "compute Terrier features");

							// get the entry with that docid (and qid). If not found put everything to 0
							double[] terrierArray;
							if(terrierHashMap.containsKey(docid))
								terrierArray = terrierHashMap.get(docid);
							else terrierArray = new double[count_terrier_features];				

							/**
							 * Now we have to compute and write all other features..
							 */

							/*-----------------------------------------------------------------------
							 * 	Compute array(s) termFrequencies 
							 *-----------------------------------------------------------------------*/
							long startTime4 = 0;
							if(Execution.logger.isTraceEnabled()) // we don't want to get time info if we are in not in trace mode
								startTime4 = System.currentTimeMillis();
							
							for (int m = 0; m < queryTerms.length; m++) {
								try {
									termFreqs[m] = Utility.getDocumentTermFrequency(queryTerms[m], docid, pi, doi, lex);
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
							/**
							 * Compute termFrequencies array on a particular field. fieldTermFreqs is made by pairs
							 * (field_index, array_frequencies[])
							 * 
							 * For each field in fieldIndexes (in fieldIndexes we have the fields we are interested in)
							 */
							for (int r = 0; r < fieldIndexes.size(); r++) { // for each field we found parsing features
																	
								int field = fieldIndexes.get(r); // get the field number
								double[] tempArray = new double[queryTerms.length]; // initialize the array which will have term frequencies for that field
								for (int m = 0; m < queryTerms.length; m++) { // compute the array

									try {
										tempArray[m] = Utility.getDocumentFieldTermFrequency(queryTerms[m], docid, pi, doi, lex, field); 
									} catch (IOException e) {
										e.printStackTrace();
									}
								}
								fieldTermFreq.put(field, tempArray); // this hashmap is something like (0,[2 5 1]; 1,[2 0 1];...)
							}
							if(Execution.logger.isTraceEnabled()) 
								Utility.getTime(startTime4, "compute arrays termFrequencies");

							/*---------------------------------------------------------
							 *  Compute TF_IDF Array
							 *---------------------------------------------------------*/
							long startTimeTFIDF = 0;
							if(Execution.logger.isTraceEnabled()) // we don't want to keep time info if we are in not in trace mode
								startTimeTFIDF = System.currentTimeMillis();
							int field = -1;
							tf_idf = Retrieval.getTF_IDF(index, docid, termFreqs, queryTerms, doi, lex, field);

							// Compute tfidf for the others fields
							for (int r = 0; r < fieldIndexes.size(); r++) {
								field = fieldIndexes.get(r); // get the field number
								double[] tempArray = new double[queryTerms.length]; // initialize the array which will have term frequencies for that field

								// we have to use term_freq for that particular field 
								double[] fieldFreq = fieldTermFreq.get(field); // get the termFreqs array for that particular field
								tempArray = Retrieval.getTF_IDF(index, docid, fieldFreq, queryTerms, doi, lex, field);

								fieldTFIDF.put(field, tempArray); // this hashmap is something like (0,[2 5 1]), (1,[2 0 1]), ...
							}
							if(Execution.logger.isTraceEnabled()) 
								Utility.getTime(startTimeTFIDF, "compute arrays TFIDF");

							/*---------------------------------------------------------
							* For every FEATURE in Features.features
							*---------------------------------------------------------*/
							int necessary_bytes = 4 + Features.num_integer_features * 4 + Features.num_double_features * 8; // first 4 bytes are for relevance judgment, 4 bytes for integers and 8 bytes for double
		
							byte[] bytesToWrite = new byte[necessary_bytes]; // this will be filled with computedbytes, only when all feature values are computed we proceed writing on disk
							int index_temp = 0; // keep track of which byte i have to write
							int terrier_features_count = 0;

							// relevance judgment
							byte[] tempRel = Utility.intToByteArray(relJud);
							for (int i = 0; i < tempRel.length; i++) {
								bytesToWrite[index_temp] = tempRel[i];
								index_temp++; // updated every time i write a byte.. at the end it will be = necessaryBytes
							}
							
							long featuresTime = 0;
							if(Execution.logger.isTraceEnabled()) 
								featuresTime = System.currentTimeMillis();
							// For each computed feature
							for (int k = 0; k < Features.features.size(); k++) {
								Features feature = Features.features.get(k);
								String classname = feature.class_name;
								if (!classname.startsWith("terrier.")) { // it is not a Terrier feature

									String data_type = feature.data_type;
									int fieldIndex = feature.field_index;

									if (fieldIndex == -1) { // Whole document feature
										Class<?> clazz;
										clazz = Class.forName(classname);
										Constructor<?> constructor = clazz.getConstructor(Index.class, Integer.TYPE, Integer.TYPE, String[].class,double[].class, double[].class);
										Scoring instance = (Scoring) constructor.newInstance(index, docid,fieldIndex, queryTerms, termFreqs, tf_idf);
										// We write the score on raf according to the type of the feature (no memory wasted)
										if (data_type.equalsIgnoreCase("java.lang.Integer")) {
											int score = (int) instance.score();
											byte[] temp = Utility.intToByteArray(score); // convert from int to 4 bytes number
											for (int i = 0; i < temp.length; i++) {
												bytesToWrite[index_temp] = temp[i];
												index_temp++; 
											}

										} else if (data_type.equalsIgnoreCase("java.lang.Double")) {
											double score = instance.score();
											byte[] temp = Utility.doubleToByteArray(score); // convert from double to 8 bytes number
											for (int i = 0; i < temp.length; i++) {
												bytesToWrite[index_temp] = temp[i];
												index_temp++;
											}
										}
									}

									else { // fieldIndex != -1 => field feature
										Class<?> clazz;
										clazz = Class.forName(classname);
										Constructor<?> constructor = clazz.getConstructor(Index.class, Integer.TYPE,Integer.TYPE, String[].class, double[].class, double[].class);
										double[] tempFieldFreq = fieldTermFreq.get(fieldIndex); // get the termFreqs for that particular field							
										double[] tempTFIDF = fieldTFIDF.get(fieldIndex);
										Scoring instance = (Scoring) constructor.newInstance(index, docid,fieldIndex, queryTerms,tempFieldFreq, tempTFIDF);
										// We write the score on raf according to the type of the feature (no memory wasted)
										if (data_type.equalsIgnoreCase("java.lang.Integer")) {
											int score = (int) instance.score();
											byte[] temp = Utility.intToByteArray(score);
											for (int i = 0; i < temp.length; i++) {
												bytesToWrite[index_temp] = temp[i];
												index_temp++;
											}

										} else if (data_type.equalsIgnoreCase("java.lang.Double")) {
											double score = instance.score();
											byte[] temp = Utility.doubleToByteArray(score);
											for (int i = 0; i < temp.length; i++) {
												bytesToWrite[index_temp] = temp[i];
												index_temp++;
											}
										}
									}
								} else { // terrier feature
									byte[] temp = Utility.doubleToByteArray(terrierArray[terrier_features_count]);
									for (int p = 0; p < temp.length; p++) {
										bytesToWrite[index_temp] = temp[p];
										index_temp++;
									}
									terrier_features_count++;
								}
							}
						
							long writeTime = 0;
							if(Execution.logger.isTraceEnabled()) 
								Utility.getTime(featuresTime, "compute other features");
								writeTime = System.nanoTime();
							raf.write(bytesToWrite); // write all byte buffer on file.. done only once for every document at the end of the computations, not every I compute a feature
							if(Execution.logger.isTraceEnabled()) 
								Utility.getTimeNano(writeTime, "write whole byte array");

						}

						/*-----------------------------------------------------------------------
						 * Already computed features for docid/qid 
						 *-----------------------------------------------------------------------*/
						else {
//							count_match++;
							 Execution.logger.debug("Already computed values for " + t + "-th entry. Document: "+ docno +". Run num."+ u);
						}

						fieldTermFreq.clear();
						fieldTFIDF.clear();

					} // end document present in the index (no error) case
					if(Execution.logger.isTraceEnabled()) {
						Utility.getTime(startTime, "Features computation for this docno");
						Execution.logger.trace("---------------------");
					}

				
				} // end for on docnos
				listDocno.clear();

			} // next we will process next qid (end for on run names)

			/*-----------------------------------------------------------------------
			 * Write hashMap with pointers to position on file in /memoryPointers/" + qid
			 *-----------------------------------------------------------------------*/
			Utility.writeHashMapFile(pointerMemory, actualQID);

//			 //-- DEBUG: FUNZIONA SOLO X IL 351 ---
//			 long addr2 = pointerMemory.get("FT934-4856");
//			 System.out.println("docno: FT934-4856 " + " posizione: " + addr2)
//			 ;
//			 raf.seek(addr2);
//			 System.out.println("REL: " + raf.readInt());
//			 for (int i=0; i<Features.features.size() ;i++) {
//			 if(Features.features.get(i).data_type.equals("java.lang.Double"))
//			 System.out.println(i + ":" + raf.readDouble());
//			 else System.out.println(i + ":" + raf.readInt());
//			
//			 }
//			 System.exit(0);
			
			/*-----------------------------------------------------------------------
			 * Clear terrierHashMap and pointerMemory for new qid
			 *-----------------------------------------------------------------------*/
			terrierHashMap.clear();
			pointerMemory.clear();

			raf.close();

			
//			long timeTime2 = System.currentTimeMillis();
//			long totalTime = timeTime2 - timeTime;
//			double totalTimeSec = (double) totalTime / 1000;			
			
			actualQID++;
			
		}
		
//		long TOTtimeTime2 = System.currentTimeMillis();
//		long totalTime3 = TOTtimeTime2 - TOTtimeTime;
//		double totalTimeSec3 = (double) totalTime3 / 1000;
	
		
		
				
	}

}
