package it.unipd.dei.ims.FeaturesExtraction.terrier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;

import org.apache.log4j.Level;
import org.apache.log4j.PropertyConfigurator;
import org.terrier.matching.ResultSet;
import org.terrier.structures.BitIndexPointer;
import org.terrier.structures.DocumentIndex;
import org.terrier.structures.DocumentIndexEntry;
import org.terrier.structures.FieldDocumentIndexEntry;
import org.terrier.structures.Index;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;
import org.terrier.structures.MetaIndex;
import org.terrier.structures.PostingIndex;
import org.terrier.structures.postings.IterablePosting;
import org.terrier.structures.postings.bit.BlockFieldIterablePosting;


/** This class contains utility methods
 * 
 * @author	Paolo Picello
 * @email	paolo.picello@studenti.unipd.it
 * 
 *
 */
public class Utility {
	
	
	/************************************************************/
	/********************  INDEX METHODS ************************/
	/************************************************************/
	
	/**
	* Returns how many documents have a particular term (in quanti documenti appare)
	* @param term The term to search for
	* @param lex The Lexicon.
	* @return The number of documents in which term appears
	*/
	//How many documents does term X occurs in? */
	public static int howManyDocumentsWithTerm (String term, Lexicon<String> lex) {
		LexiconEntry le = lex.getLexiconEntry(term);
		if (le != null) {
			//System.out.println("Term " + term + " occurs in "+ le.getDocumentFrequency() + " documents");
			return le.getDocumentFrequency();
		}
		else {
			//System.out.println("Term does not occur");
			return 0;
		}
	}
	
	/**
	* Returns the frequency of a term in the collection (quante volte appare in totale)
	* @param term the term to search for
	* @param lex The lexicon.
	* @return the frequency of the term in the collection
	*/
	public int getTermFrequencyCollection (String term, Lexicon<String> lex) {
		LexiconEntry le = lex.getLexiconEntry(term);
		int freq = 0;
		if (le != null) {
			freq = le.getFrequency();
		}
		return freq;
	}
	
	/**
	* Print the probability of a term in the collection.
	* @param term the term.
	*/
	//What is the probability of term Y occurring in the collection? */
	public void probabilityOfTerm (String term, Lexicon<String> lex, Index index) {
		LexiconEntry le = lex.getLexiconEntry(term);
		double p = le == null 
			?  0.0d
			: (double) le.getFrequency() / index.getCollectionStatistics().getNumberOfTokens();
		System.out.println("Probability of term " + term + " = " + p);
	}
	
	/**
	* Print statistics of the collection: avg_length, total_docs and total_tokens
	* @param index The index.
	*/
	//Print statistics of the collection (avg length, total_docs, total_tokens) */
	public static void getStatistics(Index index) {
		int total_docs = index.getCollectionStatistics().getNumberOfDocuments();
		System.out.println("avg length " + index.getCollectionStatistics().getAverageDocumentLength());
		System.out.println("Number of documents: "+ total_docs);
		long total_tokens = index.getCollectionStatistics().getNumberOfTokens();
		System.out.println("Number of tokens: "+ total_tokens);
	}
	
	
	/**
	* Print terms in a particular document given its docId
	* @param docid The document id.
	* @param pi The Direct Index.
	* @param doi The Document index.
	* @param lex The Lexicon.
	* @throws IOException if cannot find the document.
	*/
	//What terms occur in the docid_th document? */
	public static void getDocumentTerms(int docid, PostingIndex pi, DocumentIndex doi,  Lexicon<String> lex) throws IOException {
		IterablePosting postings = pi.getPostings((BitIndexPointer)doi.getDocumentEntry(docid));
		System.out.println("Terms in document with id=" + docid + " length= " + postings.getDocumentLength());
		while (postings.next() != IterablePosting.EOL) {
			Map.Entry<String,LexiconEntry> lee = lex.getLexiconEntry(postings.getId());
			System.out.print(lee.getKey() + " tf=" + postings.getFrequency() + " dl=" + postings.getDocumentLength() + ";");
		}
		System.out.println("");
	}
	
	/**
	* Return the frequency of a term in a particular document. Scanning all the terms in the document (faster than search for the documents in which that term is).
	* Scansiona tutti i termini in un documento e quando lo trovo mi da la frequency. Più veloce che scansionando i documenti in cui appare il termine fino a quando non trovo quel documento.
	* @param term The term.
	* @param docid The document id.
	* @param pi The Direct Index.
	* @param doi The Document index.
	* @param lex The Lexicon.
	* @throws IOException if cannot find the document.
	* @return the frequency of a term in this document
	*/
	public static double getDocumentTermFrequency (String term, int docid, PostingIndex pi, DocumentIndex doi, Lexicon<String> lex) throws IOException {
		IterablePosting postings = pi.getPostings((BitIndexPointer)doi.getDocumentEntry(docid));
		double frequency = 0;
		while (postings.next() != IterablePosting.EOL) {
			Map.Entry<String,LexiconEntry> lee = lex.getLexiconEntry(postings.getId());
			if (lee.getKey().equals(term) ) {
				frequency = postings.getFrequency();
				break;
			}
		}
		return frequency;
	}
	
	/** Scanning all documents with a particular term (SLOOOOWER - Verified!) */
	public static double getDocumentTermFrequency2 (String term, String docnoobj, PostingIndex inv, DocumentIndex doi, Lexicon<String> lex, MetaIndex meta) throws IOException {
		LexiconEntry le = lex.getLexiconEntry(term);
		IterablePosting postings = inv.getPostings((BitIndexPointer) le);
		double frequency = 0;
		while (postings.next() != IterablePosting.EOL) {
			String docno = meta.getItem("docno", postings.getId());
			if(docno.equals(docnoobj)) {
				frequency = postings.getFrequency();
				break;
			}				
		}
		return frequency;
	}
	
	
	/**
	* Return the length of a document given its id
	* @param docid The document id.
	* @param doi The Document index.
	* @throws IOException if cannot find the document.
	* @return the document length
	*/
	public static int getDocumentLength(int docid, DocumentIndex doi ) throws IOException {
		DocumentIndexEntry die = doi.getDocumentEntry(docid);
		return die.getDocumentLength();
	}
	
		
	/**
	* Print documents DOCNO with a particular term and its frequency in those documents
	* @param term The term.
	* @param lex The Lexicon.
	* @param inv The Inverted index
	* @throws IOException if cannot find the term.
	*/
	//In which documents does term Z occurs in? 
	public void getDocumentsWithTerm(String term, Lexicon<String> lex, PostingIndex inv, MetaIndex meta ) throws IOException {
		LexiconEntry le = lex.getLexiconEntry(term);
		IterablePosting postings = inv.getPostings((BitIndexPointer) le);
		while (postings.next() != IterablePosting.EOL) {
			String docno = meta.getItem("docno", postings.getId());
			System.out.println(docno + " with frequency " + postings.getFrequency());
		}
	}
	

	/**
	* Return a list of documents in which appears a term
	* @param term The term..
	* @param lex The Lexicon.
	* @param inv The Inverted index.
	* @param meta The Meta Index.
	* @throws IOException if cannot find the term.
	* @return a List<String> with all the DOCNOs of documents in which term apppears
	*/
	//In which documents does term Z occurs in? 
	public static List<String> getListDocumentsWithTerm(String term, Lexicon<String> lex, PostingIndex inv, MetaIndex meta ) throws IOException {
		List<String> documents = new ArrayList<String>();
		LexiconEntry le = lex.getLexiconEntry(term);
		IterablePosting postings = inv.getPostings((BitIndexPointer) le);
		while (postings.next() != IterablePosting.EOL) {
//			System.out.println("id of doc: " + postings.getId());
			String docno = meta.getItem("docno", postings.getId());
//			System.out.println(docno + " with frequency " + postings.getFrequency());
//			String temp=""+postings.getId();
			documents.add(docno);
//			documents.add(temp);
		}
		return documents;
	}
	
	/**
	* Return the docid of a document given its DOCNO
	* @param docno the DOCNO.
	* @param doi The Document index.
	* @param meta The Meta Index.
	* @throws IOException if cannot find the term.
	* @throws NoSuchElementException if cannot find the term.
	* @return an int which is the docid of that document
	*/
	public static int getDocumentIDFromDOCNO(String docno, DocumentIndex doi, MetaIndex meta) throws IOException, NoSuchElementException {
		int docid;
		docid = meta.getDocument("docno", docno);
		//System.out.println("MATCH FOUND FOR DOCNO: " + docno + " @id: " + docid);
		return docid;
	}
	
	/************************************************************/
	/******************** PER FIELD METHODS *********************/
	/************************************************************/	 
	/**
	* Prints the length of each field (mi stampa la lunghezza di ogni field)
	* @param doi The Document index.
	* @param docid The document id.
	* @throws IOException if cannot find the document.
	*/
	public static void printDocumentFieldLength (DocumentIndex doi, int docid) throws IOException {
		FieldDocumentIndexEntry die = (FieldDocumentIndexEntry) doi.getDocumentEntry(docid);
		int[] ffreq = die.getFieldLengths();
        for (int i = 0; i < ffreq.length; i++) {
        	System.out.println("Length of the " + i + "-th field: " + ffreq[i]);
        }
     }
	
	/**
	* Prints the length of each field (mi stampa la lunghezza di ogni field)
	* @param id The index of the field.
	* @param docid The document id.
	* @param doi the DocumentIndex
	* @throws IOException if cannot find the document.
	*/
	public static int getDocumentFieldLengthbyId(int id, int docid, DocumentIndex doi) throws IOException {
		FieldDocumentIndexEntry die = (FieldDocumentIndexEntry) doi.getDocumentEntry(docid);
		int[] ffreq = die.getFieldLengths();
		return ffreq[id];
	}
	
	/**
	* Prints the total frequency for every field (mi stampa la lunghezza di ogni field)
	* @param term The term.
	* @param docid The document id.
	* @param pi The direct index.
	* @param doi The Document Index
	* @param lex The lexicon
	* @throws IOException if cannot find the document.
	*/
	public static void printDocumentFieldTermFrequency (String term, int docid, PostingIndex pi, DocumentIndex doi, Lexicon<String> lex) throws IOException {
		BlockFieldIterablePosting postings =  (BlockFieldIterablePosting) pi.getPostings((FieldDocumentIndexEntry) doi.getDocumentEntry(docid));
		while (postings.next() != IterablePosting.EOL) {
			Map.Entry<String,LexiconEntry> lee = lex.getLexiconEntry(postings.getId());
			if (lee.getKey().equals(term) ) {
				int[] ffreq = postings.getFieldFrequencies();
				for (int i = 0; i < ffreq.length; i++) {
		        	System.out.println("Freq of "+ term + " in " + i + "-th field: " + ffreq[i]);
		        }
			}
		}
	}	
	
	/**
	* Gives me the frequency of a term in a particular field of a particular document
	* @param term The term.
	* @param docid The document id.
	* @param pi The direct index.
	* @param doi The Document Index
	* @param lex The lexicon
	* @param fieldIndex the index of the field fo which we want the frequency 
	* @throws IOException if cannot find the document.
	*/
	public static int getDocumentFieldTermFrequency (String term, int docid, PostingIndex pi, DocumentIndex doi, Lexicon<String> lex, int fieldIndex) throws IOException {
		BlockFieldIterablePosting postings =  (BlockFieldIterablePosting) pi.getPostings((FieldDocumentIndexEntry) doi.getDocumentEntry(docid));
		while (postings.next() != IterablePosting.EOL) {
			Map.Entry<String,LexiconEntry> lee = lex.getLexiconEntry(postings.getId());
			if (lee.getKey().equals(term) ) {
				int[] ffreq = postings.getFieldFrequencies();
				return ffreq[fieldIndex];
			}
		}
		return 0;
	}	
	
	/** Scanning all documents with a particular term (SLOOOOWER) */
	public static double getDocumentFieldTermFrequency2 (String term, String docnoobj, PostingIndex inv, DocumentIndex doi, Lexicon<String> lex, MetaIndex meta, int fieldIndex) throws IOException {
		LexiconEntry le = lex.getLexiconEntry(term);
		BlockFieldIterablePosting postings = (BlockFieldIterablePosting) inv.getPostings((BitIndexPointer) le);
		while (postings.next() != IterablePosting.EOL) {
			String docno = meta.getItem("docno", postings.getId());
			if(docno.equals(docnoobj)) {
				int[] ffreq = postings.getFieldFrequencies();
				return ffreq[fieldIndex];
			}				
		}
		return 0;
	}
	
	/************************************************************/
	/*******************  UTILITY METHODS ***********************/
	/************************************************************/
	
	/**
	 * Simply put a value in a particular index of an array. Equals to "array[position]=value"
	 * @param value The value.
	 * @param position The index in which to put the value
	 * @param array The array.
	 * @return An array containing the name of the files in this directory
	 */
	public static double[] putValueInVector(double value, int position, double[] array) {
		array[position]=value;
		return array;	
	}
	
	/**
	 * Return file names in a particular directory
	 * @param path The path of the directory
	 * @return An array containing the name of the files in this directory
	 */
	public static String[] listContents (String path) {
			File file = new File(path);
			String[] files = file.list(new FilenameFilter() {

			    @Override
			    public boolean accept(File dir, String name) {
		            return !name.equals(".DS_Store");
			    }
			});
			return files;
	}
	
	/**
	 * Return the num of files in a particular folder
	 * @param path The path of the directory
	 * @return The numer of files in the directory
	 */
	public static int getNumDocInFolder(String path) {
		File file = new File(path);
		String[] files = file.list(new FilenameFilter() {

		    @Override
		    public boolean accept(File dir, String name) {
	            return !name.equals(".DS_Store");
		    }
		});
		return files.length;
	}
	
	
	/**
	* Populate an hashMap putting a value in a particular index of the array. The docid will be the key and an array of double will be the value
	* @param position The index for the position in which to put the value.
	* @param rs The ResultSet.
	* @param hashMap The hashMap in which to put the value.
	* @param numFeatures The total number of features
	*/
	public static void populateHashMap( int position, ResultSet rs, HashMap<Integer, double[]> hashMap, int numFeatures) {
		double[] res = rs.getScores();
		//StaTools.standardNormalisation(rs.getScores()); //se voglio normalizzare come per Normalised2LETOROutputFormat (invece no vengono diversi verificare)
		int[] docIds = rs.getDocids();

		//popolo hash map x questa query 
		for(int i = 0; i < rs.getExactResultSize(); i++) {
		    double[] arrayInMap = hashMap.get(docIds[i]);
			if (arrayInMap == null) {
				double featuresVector[] = new double[numFeatures];
				featuresVector[position] = res[i];
		        hashMap.put(docIds[i], featuresVector);
		    } else {
		        if (arrayInMap.length != numFeatures) {
		            throw new IllegalStateException("Key "+ docIds[i] + ": cannot add " + numFeatures
		                    + " values to array of length " + arrayInMap.length);
		        }
//		        double[] featuresVector=hashMap.get(docIds[i]);
				Utility.putValueInVector(res[i], position, arrayInMap);
		        hashMap.put(docIds[i], arrayInMap);
		    }
		}
	}
	
	/**
	* Print the array for a particular key of the hashMap (The docid will be the key)
	* @param docid The docid for the entry i want to print.
	* @param rs The ResultSet.
	* @param hashMap The hashMap.
	*/
	public static void printHashMap (int docid, HashMap<Integer, double[]> hashMap) {
		double[] featuresVector3;
        featuresVector3 = hashMap.get(docid);
        for(int i = 0; i < featuresVector3.length; i++) {
        	System.out.print("" + i + "=" + featuresVector3[i] + " ");
        }
        System.out.println("");
	}
	
	/**
	* Return the number of lines in a file
	* @param filename The filename.
	* @return The number of lines.
	*/
	public static int countLines(String filename) throws IOException {
	    BufferedReader reader = new BufferedReader(new FileReader(filename));
	    int cnt = 0;
	    String lineRead = "";
	    while ((lineRead = reader.readLine()) != null) {
	    	cnt++;
	    }
	    reader.close();
	    return cnt;
	}
	
	/**
	* DEPRECATED: this is from version 0.1 in which featrues are in a different file 
	* Reads feature names from file.
	* @param filename The filename.
	* @param featuresNames the array which will contain the names.
	* @return an array containing the content of the lines in the file.
	*/
	public static String[] readFromFile(String filename, String[] featuresNames) throws IOException {
	    BufferedReader reader  = new BufferedReader(new FileReader(filename));
	    String lineRead = "";
	    int i = 0;
	    while ((lineRead = reader.readLine()) != null) {
	    	featuresNames[i] = lineRead;
	    	i++;
	    }
	    reader.close();
	    return featuresNames;
	}	
	
	/**
	* DEPRECATED 
	* Questo metodo mi dice tutti i fields che sono considerati tra le mie features.
	* @param hashMap The hashMap to be written.
	* @param qid The qid for this hashMap.
	*/
	public static ArrayList<Integer> getFieldsFromFeatures (String filename, ArrayList<Integer> featuresFields) throws IOException {
		BufferedReader reader  = new BufferedReader(new FileReader(filename));
	    String lineRead = "";
	    while ((lineRead = reader.readLine()) != null) {
	    	if(Character.isDigit(lineRead.charAt(lineRead.length()-1)) && !lineRead.startsWith("WM:")) { //PER FIELD
				int fieldIndex = lineRead.charAt(lineRead.length()-1) - '0';
				if(!featuresFields.contains(fieldIndex))
				featuresFields.add(fieldIndex);
	    	}
	    }
	    reader.close();
	    return featuresFields;
	}
	
	/**
	* Writes the content of a particular hashMap in a file ("resources/memoryPointers/"+qid) .
	* @param hashMap The hashMap to be written.
	* @param qid The qid for this hashMap.
	*/
	public static void writeHashMapFile ( HashMap<String, Long> hashMap, int qid ) throws IOException {
		String path = Execution.pointers_path + "/" + qid;

		//stampo su file
		File fileOne=new File(path);
		fileOne.delete(); //cancello il file precedente se esiste
	
	    FileOutputStream fos=new FileOutputStream(fileOne);
	    ObjectOutputStream oos=new ObjectOutputStream(fos);
	    oos.writeObject(hashMap);
	    oos.flush();
	    oos.close();
	    fos.close();
	}
	
	/**
	* Read an hashmap from file and populate a new hashmap with those values
	* @param hashMap The hashmap that will be filled
	* @param qid The qid indicates which hashmap we're interested
	*/
	public static void readFromHashMapFile (HashMap<String, Long> hashMap, int qid) throws IOException, ClassNotFoundException {
		String path= "resources/memoryPointers/" + qid;
		File toRead=new File(path);
        FileInputStream fis=new FileInputStream(toRead);
        ObjectInputStream ois=new ObjectInputStream(fis);

        @SuppressWarnings("unchecked")
		HashMap<String,Long> mapInFile=(HashMap<String,Long>)ois.readObject();

        ois.close();
        fis.close();
        //print All data in MAP
        for(Map.Entry<String,Long> m : mapInFile.entrySet()){
            System.out.println(m.getKey() + " : " + m.getValue());
        }
	}
	
	
	/**
	* Delete the content of a folder.
	* @param folder The folder which content will be deleted.
	*/
	public static void deleteFolder(File folder) {
	    File[] files = folder.listFiles();
	    if(files!=null) { //some JVMs return null for empty dirs
	        for(File f: files) {
	            if(f.isDirectory()) {
	                deleteFolder(f);
	            } else {
	                f.delete();
	            }
	        }
	    }
	    //folder.delete(); //uncomment this line if you want to delete also the folder
	}
	
	public static void getTime (long startTime, String message) {
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		double totalTimeSec = (double)totalTime;
		Execution.logger.trace("Time to " + message + ": " + totalTimeSec + " ms");
	}
	
	public static void getTimeNano (long startTime, String message) {
		long endTime = System.nanoTime();
		long totalTime = endTime - startTime;
		double totalTimeSec = (double)totalTime;
		Execution.logger.trace("Time to " + message + ": " + totalTimeSec + " ns");
	}
	
	/** 
	 * Converts an integer number in a 4 byte array
	 * @param value The int to be trasformed
	 * @return the 8 byte array representing this number
	 */
	public static byte[] intToByteArray(int value) {
		// >>> : The left operands value is moved right by the number of bits specified by the right operand and shifted values are filled up with zeros
	    return new byte[] {
	            (byte)(value >>> 24), // converto in byte 8 bit alla volta
	            (byte)(value >>> 16),
	            (byte)(value >>> 8), // shifta i bit in value di 8 posizioni a destra
	            (byte)value};
	}
	
	/** 
	 * Converts a double number in a 8 byte array
	 * @param value The double to be trasformed
	 * @return the 8 byte array representing this number
	 */
	public static byte[] doubleToByteArray(double value) {
		long tmp = Double.doubleToLongBits(value);
		byte[] result = new byte[8];
		for (int i = 7; i >= 0; i--) {
			// When you do a bit-wise AND with 0xFF(255) on any number, it is going to mask(make ZEROs) all but the lowest 8 bits of the number
			result[i] = (byte)(tmp & 0xFF); // hexadecimal FF is 255, bitwise AND (prendo gli 8 bit + a destra di tmp e li metto in result[i]?
		    tmp >>= 8; // shifta i bit in tmp di 8 posizioni a destra
		}
		return result;
	}

	/**
	 * Return the logger Level given the level as a String
	 * @param logger_level the level as String
	 * @return
	 */
	public static Level getLoggerLevel(String logger_level) {
		// TODO Auto-generated method stub
		if(logger_level.equalsIgnoreCase("TRACE"))
			return Level.TRACE;
		else if(logger_level.equalsIgnoreCase("DEBUG"))
			return Level.DEBUG;
		else if(logger_level.equalsIgnoreCase("INFO"))
			return Level.INFO;
		else if(logger_level.equalsIgnoreCase("WARN"))
			return Level.WARN;
		else if(logger_level.equalsIgnoreCase("ERROR"))
			return Level.ERROR;
		else if(logger_level.equalsIgnoreCase("FATAL"))
			return Level.FATAL;
		
		else 
			return null;
	}
	
	public static void configureLogger() {
		Properties properties=new Properties();
	    properties.setProperty("log4j.rootLogger","TRACE,stdout,MyFile");
	    properties.setProperty("log4j.rootCategory","TRACE");

	    properties.setProperty("log4j.appender.stdout", "org.apache.log4j.ConsoleAppender");
	    properties.setProperty("log4j.appender.stdout.layout",  "org.apache.log4j.PatternLayout");
	    properties.setProperty("log4j.appender.stdout.layout.ConversionPattern","%d{yyyy/MM/dd HH:mm:ss.SSS} [%1p] %t (%F) - %m%n");

	    properties.setProperty("log4j.appender.MyFile", "org.apache.log4j.RollingFileAppender");
	    properties.setProperty("log4j.appender.MyFile.File", "logs/last_execution.log"); //set log for last execution
	    properties.setProperty("log4j.appender.MyFile.MaxFileSize", "10000KB");
	    properties.setProperty("log4j.appender.MyFile.MaxBackupIndex", "1");
	    properties.setProperty("log4j.appender.MyFile.layout",  "org.apache.log4j.PatternLayout");
	    properties.setProperty("log4j.appender.MyFile.layout.ConversionPattern","%d{yyyy/MM/dd HH:mm:ss.SSS} [%1p] %t (%F) - %m%n");
	    properties.setProperty("log4j.appender.MyFile.Append","false");

	    PropertyConfigurator.configure(properties);
	}
	
}
