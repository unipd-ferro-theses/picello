package it.unipd.dei.ims.FeaturesExtraction.terrier;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;


/** This class parses pool and extract the relevance judgment ahshmap
 * 
 * @author 	Paolo Picello
 * @email	paolo.picello@studenti.unipd.it
 *
 */
public class Pool {

	String queryID;
	String docNO;	
	String model;
	float value;
		
	 static HashMap<String, Integer> hashMap = new HashMap<String, Integer>(); 

	 /**
	 * Read pool from file
	 * Create an hashMap with key in the form "docno/qid" and value Integer rapresenting the relevance
	 * @param in BufferedReader for the file.
	 * @throws IOException if cannot read the pool.
	 */
	 public static void readPool (BufferedReader in) throws IOException {
			String line = in.readLine();
			String queryID = "";
			String docNO = "";
			int relJud = 0;
				
			while(line != null) {
				String[] splitStr = line.split("\\s+");
				queryID = splitStr[0];
				docNO = splitStr[2];
				relJud = Integer.parseInt(splitStr[3]);
				
				String docqid=docNO + "/" + queryID;
				hashMap.put(docqid, relJud);
				
				line = in.readLine();
			}
	}
		
}