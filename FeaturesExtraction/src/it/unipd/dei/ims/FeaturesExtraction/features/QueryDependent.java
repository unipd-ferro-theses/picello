package it.unipd.dei.ims.FeaturesExtraction.features;

import org.terrier.structures.DocumentIndex;
import org.terrier.structures.Index;
import org.terrier.structures.Lexicon;
import org.terrier.structures.PostingIndex;
import org.terrier.terms.PorterStemmer;

public abstract class QueryDependent implements Scoring {
	
	 Index index;
	 PostingIndex<?> pi;
	 DocumentIndex doi;
	 Lexicon<String> lex;
	 int docid;
	 int field;
	 double[] termFreqs;
	 String[] terms;
	 double[] tf_idf;
	 
	PorterStemmer stemmer;
	
	
	public QueryDependent(Index index, int docid, int field, String[] terms, double[] termFreqs, double[] tf_idf) { 
	    this.index = index; 
	    this.docid = docid;
	    this.termFreqs = termFreqs;
	    this.field = field;
	    this.terms = terms;
	    this.tf_idf = tf_idf;
	    
	    pi =  index.getDirectIndex(); // returns the DirectIndex
		doi = index.getDocumentIndex(); // returns the DocumentIndex
		lex = index.getLexicon();	// returns the Lexicon	
		
    } //ora posso chiamare super(index) nelle classi che la estendono

	

}
