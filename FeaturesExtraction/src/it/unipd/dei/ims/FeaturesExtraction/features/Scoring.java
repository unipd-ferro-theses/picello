package it.unipd.dei.ims.FeaturesExtraction.features;

public interface Scoring {
	
	
	public double score();
	
}
