package it.unipd.dei.ims.FeaturesExtraction.features;

import org.terrier.structures.Index;

public class MinOfTermFrequency extends QueryDependent implements Scoring {

	
	public MinOfTermFrequency (Index index, int docid, int field, String[] terms, double[] termFreqs, double[] tf_idf) {
		super(index,docid, field, terms,termFreqs, tf_idf);
	}
	
	@Override
	public double score() {
		int minOfTermFrequency = 1000;
		
		for(int i = 0; i < termFreqs.length; i++) {
			
			if(termFreqs[i]<minOfTermFrequency) minOfTermFrequency = (int) termFreqs[i];
					
		}

		return minOfTermFrequency;

	}

}