package it.unipd.dei.ims.FeaturesExtraction.features;

import org.terrier.structures.Index;


/**
 * TF = n. occorrenze del termine nel doc/dimensione doc.
 * IDF = log(n. doc in collezione/n. doc che contengono temrine ti) 
 * TF_IDF = TF * IDF
 * 
 * @author paolopicello
 *
 */
public class SumOfTF_IDF extends QueryDependent implements Scoring {

	int docLength;
	int totalDocs;

	
	public SumOfTF_IDF (Index index, int docid, int field, String[] terms, double[] termFreqs, double[] tf_idf) {
		super(index,docid,field, terms,termFreqs, tf_idf);
	}
	
	@Override
	public double score() {
		
		double sumOfTF_IDF = 0;
		

		for(int i = 0; i < termFreqs.length; i++) {
			
			sumOfTF_IDF = sumOfTF_IDF + tf_idf[i];		
		}
		
		return sumOfTF_IDF;
	}

}