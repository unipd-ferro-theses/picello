package it.unipd.dei.ims.FeaturesExtraction.features;

import java.io.IOException;

import org.terrier.structures.Index;

import it.unipd.dei.ims.FeaturesExtraction.terrier.Utility;

public class VarianceOfDocLengthNormTermFrequency extends QueryDependent implements Scoring {

	public VarianceOfDocLengthNormTermFrequency(Index index, int docid,int field, String[] terms, double[] termFreqs, double[] tf_idf) {
		super(index, docid, field, terms, termFreqs, tf_idf);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double score() {
		
		double length = 0;
		double score = 0;
		double mean = 0;
		double variance = 0;
		double[] scores = new double[termFreqs.length];
		
		try {
			if(field==-1) //whole document
			length = Utility.getDocumentLength(docid, doi);
			else //per field feature
			length = Utility.getDocumentFieldLengthbyId(field,docid,doi);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(int i = 0; i < termFreqs.length; i++) {
			
			if(termFreqs[i]!=0) { // otherwise everything go to infinity if =0 
				scores[i] = length/termFreqs[i];
				score = score + (length/termFreqs[i]);
			}
		}
		
		mean = score / termFreqs.length;
		
		// sum_over_i [(tf[i]-mean)^2]
		for(int i = 0; i < termFreqs.length; i++) {
			variance = variance + Math.pow((scores[i]-mean),2);
		}
		
		// TODO Auto-generated method stub
		return variance;
	}

}
