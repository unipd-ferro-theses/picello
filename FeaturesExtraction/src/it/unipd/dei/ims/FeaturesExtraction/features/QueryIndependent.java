package it.unipd.dei.ims.FeaturesExtraction.features;

import org.terrier.structures.DocumentIndex;
import org.terrier.structures.Index;

public abstract class QueryIndependent implements Scoring {
	
	 Index index;
	 DocumentIndex doi;
	 int docid;
	 double[] termFreqs;
	 String[] terms;
	 int field;
	 double[] tf_idf;
	 


    public QueryIndependent(Index index, int docid, int field, String terms[], double[] termFreqs, double[] tf_idf) { 
    	this.index = index; 
    	this.docid = docid;
    	this.termFreqs = termFreqs;
    	this.field = field;
    	this.terms = terms;
    	this.tf_idf = tf_idf;
    	
		doi = index.getDocumentIndex(); // returns the DocumentIndex

    } //ora posso chiamare super(index) nelle classi che la estendono

	
}
