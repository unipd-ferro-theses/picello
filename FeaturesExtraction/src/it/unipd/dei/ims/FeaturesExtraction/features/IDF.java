package it.unipd.dei.ims.FeaturesExtraction.features;

import java.io.IOException;

import org.terrier.matching.models.WeightingModelLibrary;
import org.terrier.structures.Index;

import it.unipd.dei.ims.FeaturesExtraction.terrier.Utility;


/** This feature is Docyment Independent, i could compute this only first time for every document **/
/** IDF = log(n. doc in collezione/n. doc che contengono termine ti) */
public class IDF extends QueryDependent implements Scoring {
	
	int docLength;
	int totalDocs;

	public IDF(Index index, int docid, int field, String[] terms, double[] termFreqs, double[] tf_idf) {
		super(index, docid, field, terms, termFreqs, tf_idf);
	}

	@Override
	public double score() {
		
		int[] docWithTerm = new int[terms.length];

		try {
			docLength = Utility.getDocumentLength(docid, doi);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		totalDocs = index.getCollectionStatistics().getNumberOfDocuments();

		
		for(int m=0; m<terms.length; m++) {
			docWithTerm[m] = Utility.howManyDocumentsWithTerm(terms[m], lex);
		}
		
		double idf = 0;

		// for each term compute IDF 
		for(int i = 0; i < termFreqs.length; i++) {
			if(docWithTerm[i]!=0) { //to avoid division by 0
				idf = idf + WeightingModelLibrary.log ( totalDocs/ docWithTerm[i]);
			}
			else {
				idf = idf + 0;
			}			
		}
		
		return idf;
		
	}

}
