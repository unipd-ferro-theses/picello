package it.unipd.dei.ims.FeaturesExtraction.features;

import org.terrier.structures.Index;

public class MeanTermFrequency extends QueryDependent implements Scoring {
	

	
	public MeanTermFrequency (Index index, int docid,int field, String[] terms, double[] termFreqs,  double[] tf_idf) {
		super(index,docid, field, terms,termFreqs, tf_idf);
	}
	
	@Override
	public double score() {
		double sumOfTermFrequency = 0;

		for(int i = 0; i < termFreqs.length; i++) {
			
			sumOfTermFrequency = sumOfTermFrequency + termFreqs[i];
						
		}
		
		return sumOfTermFrequency/termFreqs.length;
	}
	
}