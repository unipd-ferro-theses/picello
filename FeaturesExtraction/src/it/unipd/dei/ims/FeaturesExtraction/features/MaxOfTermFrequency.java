package it.unipd.dei.ims.FeaturesExtraction.features;

import org.terrier.structures.Index;

public class MaxOfTermFrequency extends QueryDependent implements Scoring {


	public MaxOfTermFrequency (Index index, int docid, int field, String[] terms, double[] termFreqs, double[] tf_idf) {
		super(index,docid,field, terms,termFreqs, tf_idf);
	}
	
	@Override
	public double score() {
		int maxOfTermFrequency = 0;
		
		for(int i = 0; i < termFreqs.length; i++) {
			
			if(termFreqs[i] > maxOfTermFrequency ) maxOfTermFrequency = (int) termFreqs[i];			
		}
		
		return maxOfTermFrequency;
	}

}