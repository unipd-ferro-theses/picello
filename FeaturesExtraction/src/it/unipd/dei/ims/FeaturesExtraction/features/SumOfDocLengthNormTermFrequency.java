package it.unipd.dei.ims.FeaturesExtraction.features;

import java.io.IOException;

import org.terrier.structures.Index;

import it.unipd.dei.ims.FeaturesExtraction.terrier.Utility;

public class SumOfDocLengthNormTermFrequency extends QueryDependent implements Scoring {

	public SumOfDocLengthNormTermFrequency(Index index, int docid, int field, String[] terms, double[] termFreqs, double[] tf_idf) {
		super(index, docid, field, terms, termFreqs, tf_idf);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double score() {
		
		double length = 0;
		double score = 0;
		
		try {
			if(field==-1) //whole document
			length = Utility.getDocumentLength(docid, doi);
			else //per field feature
			length = Utility.getDocumentFieldLengthbyId(field,docid,doi);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(int i = 0; i < termFreqs.length; i++) {
			
			if(termFreqs[i]!=0) // otherwise everything go to infinity if =0 
			score = score + (length/termFreqs[i]);
			
		}
		
		
		// TODO Auto-generated method stub
		return score;
	}

}
