package it.unipd.dei.ims.FeaturesExtraction.features;


import org.terrier.structures.Index;

public class CoveredQueryTermRatio extends QueryDependent implements Scoring {
	
	
	public CoveredQueryTermRatio (Index index, int docid, int field, String[] terms, double[] termFreqs, double[] tf_idf) {
		super(index,docid, field, terms,termFreqs, tf_idf);
	}
	
	@Override
	public double score() {
 
		double coveredQueryTermNumber = 0;
	
		for(int i = 0; i < termFreqs.length; i++) {

			if(termFreqs[i] > 0) coveredQueryTermNumber ++;
						
		}
		
		return coveredQueryTermNumber/termFreqs.length;

	}

}

