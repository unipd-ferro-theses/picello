package it.unipd.dei.ims.FeaturesExtraction.features;

import org.terrier.structures.Index;


/**
 * TF = n. occorrenze del termine nel doc/dimensione doc.
 * IDF = log(n. doc in collezione/n. doc che contengono temrine ti) 
 * TF_IDF = TF * IDF
 * 
 * @author paolopicello
 *
 */
public class VarianceOfTF_IDF extends QueryDependent implements Scoring {

	int docLength;
	int totalDocs;

	
	public VarianceOfTF_IDF (Index index, int docid, int field, String[] terms, double[] termFreqs, double[] tf_idf) {
		super(index,docid,field, terms,termFreqs, tf_idf);
	}
	
	@Override
	public double score() {
		
		double score = 0;
		double mean = 0;
		double variance = 0;
		
		// per ogni termine nella collezione calcolo il TF_IDF e prendo il massimo
		for(int i = 0; i < termFreqs.length; i++) {
			
			score = score + tf_idf[i];
		}
		
		mean = score / tf_idf.length;
		
		// sum_over_i [(scores[i]-mean)^2]
		for(int i = 0; i < tf_idf.length; i++) {
			variance = variance + Math.pow((tf_idf[i]-mean),2);
		}
		
		
		
		return variance;
	}

}