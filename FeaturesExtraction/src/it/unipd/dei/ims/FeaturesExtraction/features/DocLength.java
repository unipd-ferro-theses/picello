package it.unipd.dei.ims.FeaturesExtraction.features;

import java.io.IOException;

import org.terrier.structures.Index;

import it.unipd.dei.ims.FeaturesExtraction.terrier.Utility;

public class DocLength extends QueryIndependent implements Scoring { 

	public DocLength(Index index, int docid,int field, String[] terms, double[] termFreqs, double[] tf_idf){
		super(index, docid, field, terms, termFreqs, tf_idf);
	}
	

	@Override
	public double score() {
		double length = 0;
		
		try {
			if(field==-1) //whole document
			length = Utility.getDocumentLength(docid, doi);
			else //per field feature
			length = Utility.getDocumentFieldLengthbyId(field,docid,doi);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return length;
	}
	

}
