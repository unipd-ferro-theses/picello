package it.unipd.dei.ims.FeaturesExtraction.features;

import org.terrier.structures.Index;

public class NumberOfQueryTerms extends QueryDependent implements Scoring {
	
	public NumberOfQueryTerms (Index index, int docid,int field, String[] terms, double[] termFreqs, double[] tf_idf) {
		super(index,docid, field, terms,termFreqs, tf_idf);
	}
	
	@Override
	public double score() {	
		
		 // #NumberOfQueryTerms
		return termFreqs.length;
	}

}
