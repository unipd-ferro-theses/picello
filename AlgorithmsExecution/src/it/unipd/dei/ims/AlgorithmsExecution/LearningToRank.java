package it.unipd.dei.ims.AlgorithmsExecution;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

/** This class controls the execution of Learnign To Rank libraries
 * 
 * @author	Paolo Picello 
 * @email	paolo.picello@studenti.unipd.it
 * 
 */
public class LearningToRank {

	String properties_name = "";
	String runname = "";

	// Libraries paths
	String rankLibFolder = "";
	String quickRankFolder = "";
	String jforestFolder = "";

	String library = "";
	static String test_path = "";
	String testFile = "";
	
	
	static int numFeatures;
	int index_lib;
	static int[] qidsArrayTestInt = null;
	static Properties prop;
	static File dir;
		
	public LearningToRank (String runname) {
		this.runname = runname;
	}	
	
	/**
	* Execute an algorithm for a library with given commands
	* @param library The library to use,
	* @param algo The algorithm to use.
	* @param commands The commands to use.
	* @param output_path path to test letor
	* @param prop Properties file
	* @param test_qids array of string of test qids
	* @param date The date for this execution
	*/
	public void librariesExecution (String library, String algo, String[] commands, String output_path, Properties prop, String[] test_qids) throws IOException, ClassNotFoundException, InterruptedException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		long startTime = System.currentTimeMillis();
		
		Execution.logger.info("----------------------------------");
		Execution.logger.info("Starting execution for library:" + library + ", Algo:" + algo);

		 /*------------------------------------------------------------------
		 *  LEARNING TO RANK
		 *------------------------------------------------------------------*/
		File tempScript;		
		String scorefile = "";
		
		testFile = output_path;
		
		/*---------------------------------------------------------
		 *  Read lines from Test file
		 *---------------------------------------------------------*/
		BufferedReader in_run = new BufferedReader(new FileReader(testFile));
		Letor.readLine(in_run,numFeatures); //add new line entries to Letor.lines (ci mette 0.1s)

		/*---------------------------------------------------------
		 *  LearningToRank Libraries execution
		 *---------------------------------------------------------*/	
		rankLibFolder = prop.getProperty("ranklib.path");
		quickRankFolder = prop.getProperty("quickrank.path");
		jforestFolder = prop.getProperty("jforests.path");	
		
		String folder = "";
		if(library.equals("ranklib")) {
			library = "ranklib";
			folder = rankLibFolder;
			index_lib = 2;
		}
		else if(library.equals("quickrank")) {
			library="quickrank";
			folder = quickRankFolder;
			index_lib = 0;
		}
		else if(library.equals("jforests")) {
			library = "jforests";
			folder = jforestFolder;
			index_lib = 0;
		}
		
		scorefile = Execution.out_path + "/info/" + runname + "/" + runname + "_scores_" + library + ".txt" ;
		File file = new File(scorefile);
		String score_path = file.getAbsolutePath();
		
		if(Execution.logger.isDebugEnabled()) {
			for(int i=0; i<commands.length; i++) {
				Execution.logger.debug("cmd: " + commands[i]);
			}
		}
		// Lunch commands with ProcessBuilder 
		tempScript = Utility.createTempScript(commands); // create a temporary script
		
		
		try {
			ProcessBuilder pb = new ProcessBuilder("bash", tempScript.toString()); // toString returns path to the created file
			pb.directory(new File(folder)); // set the process working directory
	        pb.inheritIO();
	        Process process = pb.start();
	        process.waitFor(); // cause the current thread to wait
		} finally {				    	
	        tempScript.delete();
		}
		
		qidsArrayTestInt = new int[test_qids.length];
		for(int i = 0; i < test_qids.length; i++) {
			qidsArrayTestInt[i] = Integer.parseInt(test_qids[i]); // int array of test qids
		}
		      
		/** Perform reranking after we have generated the score file for documents in test.letor */
		Score.reRankFromScoreFile(score_path, index_lib, qidsArrayTestInt, library, runname, algo);
		
		Execution.logger.info("Finish computation for library: " + library);
		
		Letor.lines.clear();
		file.delete(); // delete the temporary scores file
			

		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		double totalTimeSec = (double)totalTime/1000;
		Execution.logger.trace("TIME TO EXECUTE THIS LIBRARY/ALGO: " + totalTimeSec);

		Execution.logger.info("---------------");

	}
	
}

