package it.unipd.dei.ims.AlgorithmsExecution.parameters;

import java.io.File;
import java.util.Properties;

import org.apache.commons.lang.ArrayUtils;

import it.unipd.dei.ims.AlgorithmsExecution.Execution;



/** This class check if the properties file is well written and if the
 *  specified parameters are allowed.
 * 
 * @author	Paolo Picello 
 * @email	paolo.picello@studenti.unipd.it
 *
 */
public class CheckProperties {
	
	/** 
	 *  !! manca gmax !!!
	 */
	
	/** Supported Algorithms */
	final static String[] SUPPORTED_ALGORITHMS = new String[] {"mart", "ranknet", "rankboost", "adarank", "coordinateascent", "lambdamart", "listnet", "randomforest", "obvmart", "obvlambdamart", "linesearch", "gradientboosting","gradientboostingbinaryclassifier" };
	
	/** Allowed libraries */
	final static String[] MART_ALLOWED_LIBRARIES = new String[] {"ranklib", "quickrank","jforests"};
	final static String[] RANKNET_ALLOWED_LIBRARIES = new String[] {"ranklib"};
	final static String[] RANKBOOST_ALLOWED_LIBRARIES = new String[] {"ranklib", "quickrank"};
	final static String[] ADARANK_ALLOWED_LIBRARIES = new String[] {"ranklib"};
	final static String[] COORDINATEASCENT_ALLOWED_LIBRARIES = new String[] {"ranklib", "quickrank"};
	final static String[] LAMBDAMART_ALLOWED_LIBRARIES = new String[] {"ranklib", "quickrank", "jforests"};
	final static String[] LISTNET_ALLOWED_LIBRARIES = new String[] {"ranklib"};
	final static String[] RANDOMFOREST_ALLOWED_LIBRARIES = new String[] {"ranklib"};
	final static String[] OBVMART_ALLOWED_LIBRARIES = new String[] {"quickrank"};
	final static String[] OBVLAMBDAMART_ALLOWED_LIBRARIES = new String[] {"quickrank"};
	final static String[] LINESEARCH_ALLOWED_LIBRARIES = new String[] {"quickrank"};
	final static String[] REGRESSIONTREE_ALLOWED_LIBRARIES = new String[] {"jforests"};

	// [...]

	/** QuickRank allowed metrics */
	final static String[] QUICKRANK_ALLOWED_METRICS = new String[] {"DCG","NDCG","TNDCG","MAP"};
	// not an easy task for QuickRank and JForests, QuickRank metrics are in the form NDCG@k,
	// for QuickRank we perform the check
	// JForest admits metrics also in the form URiskAwareEval:1:NDCG
	// [...]
	
	/** RankLib allowed normalization */
	final static String[] RANKLIB_ALLOWED_NORM = new String[] {"zscore", "sum", "linear"};
	

	static String[] martNumberOptions = { "mart.num-trees", "mart.num-leaves", "mart.shrinkage", "mart.min-leaf-support", "mart.estop","mart.quickrank.train-cutoff", "mart.quickrank.test-cutoff", "mart.quickrank.num-thresholds", "mart.ranklib.tc" };
	static String[] gradientboostingNumberOptions = { "mart.jforests.trees.min-instance-percentage-per-leaf", "mart.jforests.trees.feature-sampling", "mart.jforests.boosting.sub-sampling", "mart.jforests.boosting.early-stopping-tolerance", "mart.jforests.trees.max-leaf-output"};
	static String[] ranknetNumberOptions = { "ranknet.epoch", "ranknet.layer", "ranknet.node", "ranknet.lr"};
	static String[] rankboostNumberOptions = { "rankboost.round", "rankboost.ranklib.tc", "rankboost.quickrank.train-cutoff", "rankboost.quickrank.test-cutoff" };
	static String[] adarankNumberOptions = { "adarank.round", "adarank.tolerance", "adarank.max" };
	static String[] coordinateascentNumberOptions = { "coordinateascent.ranklib.r", "coordinateascent.ranklib.i", "coordinateascent.ranklib.tolerance", "coordinateascent.quickrank.num-samples", "coordinateascent.quickrank.windows-size", "coordinateascent.quickrank.reduction-factor", "coordinateascent.quickrank.max-iterations", "coordinateascent.quickrank.max-failed-valid" };
	static String[] lambdamartNumberOptions = { "lambdamart.num-trees", "lambdamart.num-leaves", "lambdamart.shrinkage", "lambdamart.min-leaf-support", "lambdamart.estop", "lambdamart.jforests.trees.min-instance-percentage-per-leaf", "lambdamart.jforests.trees.min-instance-per-leaf", "lambdamart.jforests.boosting.sub-sampling", "lambdamart.jforests.trees.feature-sampling", "lambdamart.jforests.boosting.early-stopping-tolerance", "lambdamart.jforests.lambdamart.sigmoid-bins", "lambdamart.jforests.lambdamart.max-dcg-truncation","lambdamart.ranklib.tc", "lambdamart.quickrank.num-thresholds"	};		
	static String[] listnetNumberOptions = { };
	static String[] randomforestNumberOptions = {"randomforest.bag", "randomforest.srate", "randomforest.frate", "randomforest.rtype", "randomforest.tree", "randomforest.leaf", "randomforest.shrinkage", "randomforest.tc", "randomforest.mls", "randomforest.estop"};
	static String[] obvmartNumberOptions = { "obvmart.num-trees", "obvmart.num-thresholds", "obvmart.shrinkage", "obvmart.min-leaf-support", "obvmart.estop", "obvmart.tree-depth"};
	static String[] obvlambdamartNumberOptions = { "obvlambdamart.num-trees", "obvlambdamart.num-thresholds", "obvlambdamart.shrinkage", "obvlambdamart.min-leaf-support", "obvlambdamart.estop", "obvlambdamart.tree-depth"};
	static String[] linesearchNumberOptions = { "linesearch.quickrank.num-samples", "linesearch.quickrank.windows-size", "linesearch.quickrank.reduction-factor", "linesearch.quickrank.max-iterations", "linesearch.quickrank.max-failed-valid" };
	static String[] regressiontreeNumberOptions = { "regressiontree.jforests.trees.min-instance-percentage-per-leaf", "regressiontree.jforests.trees.min-instance-per-leaf", "regressiontree.jforests.trees.feature-sampling", "regressiontree.jforests.boosting.early-stopping-tolerance", "regressiontree.jforests.trees.num-leaves", "regressiontree.jforests.trees.max-leaf-output" };
	static String[] gradientboostingbinaryclassifierNumberOptions = { "gradientboostingbinaryclassifier.jforests.trees.num-leaves", "gradientboostingbinaryclassifier.jforests.trees.min-instance-percentage-per-leaf", "gradientboostingbinaryclassifier.jforests.trees.min-instance-per-leaf", "gradientboostingbinaryclassifier.jforests.trees.feature-sampling", "gradientboostingbinaryclassifier.jforests.boosting.num-trees", "gradientboostingbinaryclassifier.jforests.boosting.learning-rate", "gradientboostingbinaryclassifier.jforests.boosting.sub-sampling", "gradientboostingbinaryclassifier.jforests.boosting.early-stopping-tolerance", "gradientboostingbinaryclassifier.jforests.trees.max-leaf-output"};
	static String[] normOptions = {"mart.ranklib.norm", "ranknet.ranklib.norm", "rankboost.ranklib.norm", "adarank.ranklib.norm", "coordinateascent.ranklib.norm", "lambdamart.ranklib.norm", "listnet.ranklib.norm", "randomforest.ranklib.norm" };
	
	public static boolean checkProperties(Properties prop) {
	
		boolean correct=true;
		
		/*------------------------------------------------------------------
		 *	Check GENERAL fields
		 *------------------------------------------------------------------*/
		
		/** Check if run's folder exists */
		File runs_folder = new File(prop.getProperty("runsFolder"));
		if(!runs_folder.exists()) { //la cartella delle run non esiste
			Execution.logger.fatal("Runs folder: " + prop.getProperty("runsFolder") + " does not exist!");
			correct = false;
		}
		
		/** Check if topic's fields are actually numbers (without this control you allow also alphabetical name for qids) */
		String train_qids = prop.getProperty("train"); // get String of train topics (e.g 351,369,370,...)
		String validation_qids = prop.getProperty("validation"); // get String of validation topics
		String test_qids = prop.getProperty("test"); // get String of train topics
		String tot_qids = train_qids + "," + validation_qids + "," + test_qids; // total String of topics used, useful for ThreadPool
		String[] qidsTestArray = tot_qids.split(",");
		for(int i = 0; i < qidsTestArray.length; i++) {
			try  
			  {  
			    Integer.parseInt(qidsTestArray[i]);  
			  }  
			  catch(NumberFormatException nfe)  
			  {  
				  Execution.logger.fatal(qidsTestArray[i] +  " not an integer number. We allow only integer for qids.");
			    correct = false;  
			  }  
		}
		
		
		
		/*------------------------------------------------------------------
		 *	Check ALGORITHMS field 
		 *------------------------------------------------------------------*/
		/** ranknet.libraries */
		String algo_libraries = prop.getProperty( "algorithms");
		String[] algo_libraries2= algo_libraries.split(",");
		for(int i = 0; i < algo_libraries2.length; i++) {
			if(ArrayUtils.contains(SUPPORTED_ALGORITHMS, algo_libraries2[i]) || algo_libraries.isEmpty()) {
			}
			else {
				Execution.logger.fatal(algo_libraries2[i] + " not accepted algorithm");
				correct=false;
			}
		}
		
		
		
		/*------------------------------------------------------------------
		 *	Check MART properties
		 *------------------------------------------------------------------*/
		/** mart.libraries */
		correct = correct && checkAllowedLibraries( "mart", MART_ALLOWED_LIBRARIES, prop) ;

		/** mart.quickrank.train-metric */
		/** mart.quickrank.test-metric */
		String mart_quickrank_train_metric = prop.getProperty("mart.quickrank.train-metric");
		String mart_quickrank_test_metric = prop.getProperty("mart.quickrank.test-metric");
		if(!ArrayUtils.contains(QUICKRANK_ALLOWED_METRICS, mart_quickrank_train_metric)) {
			Execution.logger.fatal(mart_quickrank_train_metric + " not accepted value for mart.quickrank.train-metric");
			correct=false;
		}
		if(!ArrayUtils.contains(QUICKRANK_ALLOWED_METRICS, mart_quickrank_test_metric)) {
			Execution.logger.fatal(mart_quickrank_test_metric + " not accepted value for mart.quickrank.test-metric");
			correct=false;
		}
		
		/** numerical parameters */
		correct = correct && checkIsNumerical(martNumberOptions, prop);
		
		if (!(prop.getProperty("mart.ranklib.train-metric").startsWith("MAP") || prop.getProperty("mart.ranklib.train-metric").startsWith("NDCG@") ||
				prop.getProperty("mart.ranklib.train-metric").startsWith("DCG@") || prop.getProperty("mart.ranklib.train-metric").startsWith("P@")
				|| prop.getProperty("mart.ranklib.train-metric").startsWith("RR@") || prop.getProperty("mart.ranklib.train-metric").startsWith("ERR@"))) {
			Execution.logger.fatal(prop.getProperty("mart.ranklib.train-metric") + " not accepted value for mart.ranklib.train-metric");
			correct = false;
		}

		if (!(prop.getProperty("mart.ranklib.test-metric").startsWith("MAP") || prop.getProperty("mart.ranklib.test-metric").startsWith("NDCG@") ||
				prop.getProperty("mart.ranklib.test-metric").startsWith("DCG@") || prop.getProperty("mart.ranklib.test-metric").startsWith("P@")
				|| prop.getProperty("mart.ranklib.test-metric").startsWith("RR@") || prop.getProperty("mart.ranklib.test-metric").startsWith("ERR@"))) {
			Execution.logger.fatal(prop.getProperty("mart.ranklib.test-metric") + " not accepted value for mart.ranklib.test-metric");
			correct = false;
		}
		
		// Gradient Boosting Options for JForest's GBRT
		/** numerical parameters */
		correct = correct && checkIsNumerical(gradientboostingNumberOptions, prop);
		
		/** Boolean values */
		String randomized_split_mart = "";
		randomized_split_mart = prop.getProperty("mart.jforests.trees.randomized-splits");
		if( !(randomized_split_mart.equalsIgnoreCase("true") || randomized_split_mart.equalsIgnoreCase("false"))) {
			Execution.logger.fatal(randomized_split_mart + " is not a boolean value for lambdamart.jforests.trees.randomized-splits");
			correct = false;
		}
		
		/*------------------------------------------------------------------
		 *	Check RANKNET properties
		 *------------------------------------------------------------------*/
		/** ranknet.libraries */
		correct = correct && checkAllowedLibraries ( "ranknet", RANKNET_ALLOWED_LIBRARIES, prop) ;

		if (!(prop.getProperty("ranknet.ranklib.train-metric").startsWith("MAP") || prop.getProperty("ranknet.ranklib.train-metric").startsWith("NDCG@") ||
				prop.getProperty("ranknet.ranklib.train-metric").startsWith("DCG@") || prop.getProperty("ranknet.ranklib.train-metric").startsWith("P@")
				|| prop.getProperty("ranknet.ranklib.train-metric").startsWith("RR@") || prop.getProperty("ranknet.ranklib.train-metric").startsWith("ERR@"))) {
			Execution.logger.fatal(prop.getProperty("ranknet.ranklib.train-metric") + " not accepted value for ranknet.ranklib.train-metric");
			correct = false;
		}

		if (!(prop.getProperty("ranknet.ranklib.test-metric").startsWith("MAP") || prop.getProperty("ranknet.ranklib.test-metric").startsWith("NDCG@") ||
				prop.getProperty("ranknet.ranklib.test-metric").startsWith("DCG@") || prop.getProperty("ranknet.ranklib.test-metric").startsWith("P@")
				|| prop.getProperty("ranknet.ranklib.test-metric").startsWith("RR@") || prop.getProperty("ranknet.ranklib.test-metric").startsWith("ERR@"))) {
			Execution.logger.fatal(prop.getProperty("ranknet.ranklib.test-metric") + " not accepted value for ranknet.ranklib.test-metric");
			correct = false;
		}
		
		// Control numerical fields
		correct = correct && checkIsNumerical(ranknetNumberOptions, prop);

		
		/*------------------------------------------------------------------
		 *	Check RANKBOOST properties
		 *------------------------------------------------------------------*/
		/** rankboost.libraries */
		correct = correct && checkAllowedLibraries ( "rankboost", RANKBOOST_ALLOWED_LIBRARIES, prop) ;

		//rankboost.ranklib.train-metric
		//rankboost.ranklib.test-metric
		String rankboost_quickrank_train_metric = prop.getProperty("rankboost.quickrank.train-metric");
		String rankboost_quickrank_test_metric = prop.getProperty("rankboost.quickrank.test-metric");
		if(!ArrayUtils.contains(QUICKRANK_ALLOWED_METRICS, rankboost_quickrank_train_metric)) {
			Execution.logger.fatal(rankboost_quickrank_train_metric + " not accepted value for rankboost.quickrank.train-metric");
			correct=false;
		}
		if(!ArrayUtils.contains(QUICKRANK_ALLOWED_METRICS, rankboost_quickrank_test_metric)) {
			Execution.logger.fatal(rankboost_quickrank_test_metric + " not accepted value for rankboost.quickrank.test-metric");
			correct=false;
		}
		
		if (!(prop.getProperty("rankboost.ranklib.train-metric").startsWith("MAP") || prop.getProperty("rankboost.ranklib.train-metric").startsWith("NDCG@") ||
				prop.getProperty("rankboost.ranklib.train-metric").startsWith("DCG@") || prop.getProperty("rankboost.ranklib.train-metric").startsWith("P@")
				|| prop.getProperty("rankboost.ranklib.train-metric").startsWith("RR@") || prop.getProperty("rankboost.ranklib.train-metric").startsWith("ERR@"))) {
			Execution.logger.fatal(prop.getProperty("rankboost.ranklib.train-metric") + " not accepted value for rankboost.ranklib.train-metric");
			correct = false;
		}

		if (!(prop.getProperty("rankboost.ranklib.test-metric").startsWith("MAP") || prop.getProperty("rankboost.ranklib.test-metric").startsWith("NDCG@") ||
				prop.getProperty("rankboost.ranklib.test-metric").startsWith("DCG@") || prop.getProperty("rankboost.ranklib.test-metric").startsWith("P@")
				|| prop.getProperty("rankboost.ranklib.test-metric").startsWith("RR@") || prop.getProperty("rankboost.ranklib.test-metric").startsWith("ERR@"))) {
			Execution.logger.fatal(prop.getProperty("rankboost.ranklib.test-metric") + " not accepted value for rankboost.ranklib.test-metric");
			correct = false;
		}
		
		// Control numerical fields
		correct = correct && checkIsNumerical(rankboostNumberOptions, prop);
		
		
		/*------------------------------------------------------------------
		 *	Check ADARANK properties
		 *------------------------------------------------------------------*/		
		/** adarank.libraries */
		correct = correct && checkAllowedLibraries ( "adarank", ADARANK_ALLOWED_LIBRARIES, prop) ;

		if (!(prop.getProperty("adarank.ranklib.train-metric").startsWith("MAP") || prop.getProperty("adarank.ranklib.train-metric").startsWith("NDCG@") ||
				prop.getProperty("adarank.ranklib.train-metric").startsWith("DCG@") || prop.getProperty("adarank.ranklib.train-metric").startsWith("P@")
				|| prop.getProperty("adarank.ranklib.train-metric").startsWith("RR@") || prop.getProperty("adarank.ranklib.train-metric").startsWith("ERR@"))) {
			Execution.logger.fatal(prop.getProperty("adarank.ranklib.train-metric") + " not accepted value for adarank.ranklib.train-metric");
			correct = false;
		}

		if (!(prop.getProperty("adarank.ranklib.test-metric").startsWith("MAP") || prop.getProperty("adarank.ranklib.test-metric").startsWith("NDCG@") ||
				prop.getProperty("adarank.ranklib.test-metric").startsWith("DCG@") || prop.getProperty("adarank.ranklib.test-metric").startsWith("P@")
				|| prop.getProperty("adarank.ranklib.test-metric").startsWith("RR@") || prop.getProperty("adarank.ranklib.test-metric").startsWith("ERR@"))) {
			Execution.logger.fatal(prop.getProperty("adarank.ranklib.test-metric") + " not accepted value for adarank.ranklib.test-metric");
			correct = false;
		}
		
		correct = correct && checkIsNumerical(adarankNumberOptions, prop);

		
		/*------------------------------------------------------------------
		 *	Check COORDINATEASCENT properties
		 *------------------------------------------------------------------*/
		/** coordinateascent.libraries */
		correct = correct && checkAllowedLibraries ( "coordinateascent", COORDINATEASCENT_ALLOWED_LIBRARIES, prop) ;
		
		if (!(prop.getProperty("coordinateascent.ranklib.train-metric").startsWith("MAP") || prop.getProperty("coordinateascent.ranklib.train-metric").startsWith("NDCG@") ||
				prop.getProperty("coordinateascent.ranklib.train-metric").startsWith("DCG@") || prop.getProperty("coordinateascent.ranklib.train-metric").startsWith("P@")
				|| prop.getProperty("coordinateascent.ranklib.train-metric").startsWith("RR@") || prop.getProperty("coordinateascent.ranklib.train-metric").startsWith("ERR@"))) {
			Execution.logger.fatal(prop.getProperty("coordinateascent.ranklib.train-metric") + " not accepted value for coordinateascent.ranklib.train-metric");
			correct = false;
		}

		if (!(prop.getProperty("coordinateascent.ranklib.test-metric").startsWith("MAP") || prop.getProperty("coordinateascent.ranklib.test-metric").startsWith("NDCG@") ||
				prop.getProperty("coordinateascent.ranklib.test-metric").startsWith("DCG@") || prop.getProperty("coordinateascent.ranklib.test-metric").startsWith("P@")
				|| prop.getProperty("coordinateascent.ranklib.test-metric").startsWith("RR@") || prop.getProperty("coordinateascent.ranklib.test-metric").startsWith("ERR@"))) {
			Execution.logger.fatal(prop.getProperty("coordinateascent.ranklib.test-metric") + " not accepted value for coordinateascent.ranklib.test-metric");
			correct = false;
		}
		
		/** coordinateascent.quickrank.train-metric */
		/** coordinateascent.quickrank.test-metric */
		String coordinateascent_quickrank_train_metric = prop.getProperty("coordinateascent.quickrank.train-metric");
		String coordinateascent_quickrank_test_metric = prop.getProperty("coordinateascent.quickrank.test-metric");
		if(!ArrayUtils.contains(QUICKRANK_ALLOWED_METRICS, coordinateascent_quickrank_train_metric)) {
			Execution.logger.fatal(coordinateascent_quickrank_train_metric + " not accepted value for coordinateascent.quickrank.train-metric");
			correct=false;
		}
		if(!ArrayUtils.contains(QUICKRANK_ALLOWED_METRICS, coordinateascent_quickrank_test_metric)) {
			Execution.logger.fatal(coordinateascent_quickrank_test_metric + " not accepted value for coordinateascent.quickrank.test-metric");
			correct=false;
		}
				
		correct = correct && checkIsNumerical(coordinateascentNumberOptions, prop);
		
		
		/*------------------------------------------------------------------
		 *	Check LAMBDAMART properties
		 *------------------------------------------------------------------*/
		/** lambdamart.libraries */
		correct = correct && checkAllowedLibraries ( "lambdamart", LAMBDAMART_ALLOWED_LIBRARIES, prop) ;
		
		if (!(prop.getProperty("lambdamart.ranklib.train-metric").startsWith("MAP") || prop.getProperty("lambdamart.ranklib.train-metric").startsWith("NDCG@") ||
				prop.getProperty("lambdamart.ranklib.train-metric").startsWith("DCG@") || prop.getProperty("lambdamart.ranklib.train-metric").startsWith("P@")
				|| prop.getProperty("lambdamart.ranklib.train-metric").startsWith("RR@") || prop.getProperty("lambdamart.ranklib.train-metric").startsWith("ERR@"))) {
			Execution.logger.fatal(prop.getProperty("lambdamart.ranklib.train-metric") + " not accepted value for lambdamart.ranklib.train-metric");
			correct = false;
		}

		if (!(prop.getProperty("lambdamart.ranklib.test-metric").startsWith("MAP") || prop.getProperty("lambdamart.ranklib.test-metric").startsWith("NDCG@") ||
				prop.getProperty("lambdamart.ranklib.test-metric").startsWith("DCG@") || prop.getProperty("lambdamart.ranklib.test-metric").startsWith("P@")
				|| prop.getProperty("lambdamart.ranklib.test-metric").startsWith("RR@") || prop.getProperty("lambdamart.ranklib.test-metric").startsWith("ERR@"))) {
			Execution.logger.fatal(prop.getProperty("lambdamart.ranklib.test-metric") + " not accepted value for lambdamart.ranklib.test-metric");
			correct = false;
		}
		
		/** lambdamart.quickrank.train-metric */
		/** lambdamart.quickrank.test-metric */
		String lambdamart_quickrank_train_metric = prop.getProperty("lambdamart.quickrank.train-metric");
		String lambdamart_quickrank_test_metric = prop.getProperty("lambdamart.quickrank.test-metric");
		if(!ArrayUtils.contains(QUICKRANK_ALLOWED_METRICS, lambdamart_quickrank_train_metric)) {
			Execution.logger.fatal(lambdamart_quickrank_train_metric + " not accepted value for lambdamart.quickrank.train-metric");
			correct=false;
		}
		if(!ArrayUtils.contains(QUICKRANK_ALLOWED_METRICS, lambdamart_quickrank_test_metric)) {
			Execution.logger.fatal(lambdamart_quickrank_test_metric + " not accepted value for lambdamart.quickrank.test-metric");
			correct=false;
		}
		
		/** Boolean values */
		String randomized_split = "";
		String imbalance_cost_adjustment = "";
		randomized_split = prop.getProperty("lambdamart.jforests.trees.randomized-splits");
		imbalance_cost_adjustment = prop.getProperty("lambdamart.jforests.boosting.imbalance-cost-adjustment");
		if( !(randomized_split.equalsIgnoreCase("true") || randomized_split.equalsIgnoreCase("false"))) {
			Execution.logger.fatal(randomized_split + " is not a boolean value for lambdamart.jforests.trees.randomized-splits");
			correct = false;
		}
		if(! (imbalance_cost_adjustment.equalsIgnoreCase("true") || imbalance_cost_adjustment.equalsIgnoreCase("false"))) {
			Execution.logger.fatal("lambdamart.jforests.boosting.imbalance-cost-adjustment is not a boolean value");
			correct = false;
		}
		
		// !! GESTIRE IL
		// Anche la metrica per jforests devo gestire
		// lambdamart.jforests.trees.features-to-discard=null
		// lambdamart.jforests.trees.features-to-include=null
		
		String lambdamart_cost_function = prop.getProperty("lambdamart.jforests.lambdamart.cost-function");
		if(lambdamart_cost_function!=null) {
			if (!(lambdamart_cost_function.equals("cross-entropy") || lambdamart_cost_function.equals("fidelity"))) {
				correct = false;
				Execution.logger.fatal(lambdamart_cost_function + " not accepted value for lambdamart.jforests.lambdamart.cost-function [cross-entropy|fidelity]");

			}
		}
		
		correct = correct && checkIsNumerical(lambdamartNumberOptions, prop);
		
		
		/*------------------------------------------------------------------
		 *	Check LISTNET properties
		 *------------------------------------------------------------------*/
		/** listnet.libraries */
		correct = correct && checkAllowedLibraries ( "listnet", LISTNET_ALLOWED_LIBRARIES, prop) ;

		if (!(prop.getProperty("listnet.ranklib.train-metric").startsWith("MAP") || prop.getProperty("listnet.ranklib.train-metric").startsWith("NDCG@") ||
				prop.getProperty("listnet.ranklib.train-metric").startsWith("DCG@") || prop.getProperty("listnet.ranklib.train-metric").startsWith("P@")
				|| prop.getProperty("listnet.ranklib.train-metric").startsWith("RR@") || prop.getProperty("listnet.ranklib.train-metric").startsWith("ERR@"))) {
			Execution.logger.fatal(prop.getProperty("listnet.ranklib.train-metric") + " not accepted value for listnet.ranklib.train-metric");
			correct = false;
		}

		if (!(prop.getProperty("listnet.ranklib.test-metric").startsWith("MAP") || prop.getProperty("listnet.ranklib.test-metric").startsWith("NDCG@") ||
				prop.getProperty("listnet.ranklib.test-metric").startsWith("DCG@") || prop.getProperty("listnet.ranklib.test-metric").startsWith("P@")
				|| prop.getProperty("listnet.ranklib.test-metric").startsWith("RR@") || prop.getProperty("listnet.ranklib.test-metric").startsWith("ERR@"))) {
			Execution.logger.fatal(prop.getProperty("listnet.ranklib.test-metric") + " not accepted value for listnet.ranklib.test-metric");
			correct = false;
		}
		
		// !! GESTIRE IL -noeq !!
		
		correct = correct && checkIsNumerical(listnetNumberOptions, prop);

		
		/*------------------------------------------------------------------
		 *	Check RANDOMFOREST properties
		 *------------------------------------------------------------------*/
		
		/** listnet.libraries */
		correct = correct && checkAllowedLibraries ( "randomforest", RANDOMFOREST_ALLOWED_LIBRARIES, prop) ;

		if (!(prop.getProperty("randomforest.ranklib.train-metric").startsWith("MAP") || prop.getProperty("randomforest.ranklib.train-metric").startsWith("NDCG@") ||
				prop.getProperty("randomforest.ranklib.train-metric").startsWith("DCG@") || prop.getProperty("randomforest.ranklib.train-metric").startsWith("P@")
				|| prop.getProperty("randomforest.ranklib.train-metric").startsWith("RR@") || prop.getProperty("randomforest.ranklib.train-metric").startsWith("ERR@"))) {
			Execution.logger.fatal(prop.getProperty("randomforest.ranklib.train-metric") + " not accepted value for randomforest.ranklib.train-metric");
			correct = false;
		}

		if (!(prop.getProperty("randomforest.ranklib.test-metric").startsWith("MAP") || prop.getProperty("randomforest.ranklib.test-metric").startsWith("NDCG@") ||
				prop.getProperty("randomforest.ranklib.test-metric").startsWith("DCG@") || prop.getProperty("randomforest.ranklib.test-metric").startsWith("P@")
				|| prop.getProperty("randomforest.ranklib.test-metric").startsWith("RR@") || prop.getProperty("randomforest.ranklib.test-metric").startsWith("ERR@"))) {
			Execution.logger.fatal(prop.getProperty("randomforest.ranklib.test-metric") + " not accepted value for randomforest.ranklib.test-metric");
			correct = false;
		}	
				
		correct = correct && checkIsNumerical(randomforestNumberOptions, prop);
		
		
		
		/*------------------------------------------------------------------
		 *	Check OBVMART properties
		 *------------------------------------------------------------------*/
		/** obvmart.libraries */
		correct = correct && checkAllowedLibraries ( "obvmart", OBVMART_ALLOWED_LIBRARIES, prop) ;
		
		/** obvmart.quickrank.train-metric */
		/** obvmart.quickrank.test-metric */
		String obvmart_quickrank_train_metric = prop.getProperty("obvmart.quickrank.train-metric");
		String obvmart_quickrank_test_metric = prop.getProperty("obvmart.quickrank.test-metric");
		if(!ArrayUtils.contains(QUICKRANK_ALLOWED_METRICS, obvmart_quickrank_train_metric)) {
			Execution.logger.fatal(obvmart_quickrank_train_metric + " not accepted value for obvmart.quickrank.train-metric");
			correct=false;
		}
		if(!ArrayUtils.contains(QUICKRANK_ALLOWED_METRICS, obvmart_quickrank_test_metric)) {
			Execution.logger.fatal(obvmart_quickrank_test_metric + " not accepted value for obvmart.quickrank.test-metric");
			correct=false;
		}
		
		correct = correct && checkIsNumerical(obvmartNumberOptions, prop);

		
		/*------------------------------------------------------------------
		 *	Check OBVLAMBDAMART properties
		 *------------------------------------------------------------------*/
		/** obvmart.libraries */
		correct = correct && checkAllowedLibraries ( "obvlambdamart", OBVLAMBDAMART_ALLOWED_LIBRARIES, prop) ;
		
		/** obvlambdamart.quickrank.train-metric */
		/** obvlambdamart.quickrank.test-metric */
		String obvlambdamart_quickrank_train_metric = prop.getProperty("obvlambdamart.quickrank.train-metric");
		String obvlambdamart_quickrank_test_metric = prop.getProperty("obvlambdamart.quickrank.test-metric");
		if(!ArrayUtils.contains(QUICKRANK_ALLOWED_METRICS, obvlambdamart_quickrank_train_metric)) {
			Execution.logger.fatal(obvlambdamart_quickrank_train_metric + " not accepted value for obvlambdamart.quickrank.train-metric");
			correct=false;
		}
		if(!ArrayUtils.contains(QUICKRANK_ALLOWED_METRICS, obvlambdamart_quickrank_test_metric)) {
			Execution.logger.fatal(obvlambdamart_quickrank_test_metric + " not accepted value for obvlambdamart.quickrank.test-metric");
			correct=false;
		}
		
		correct = correct && checkIsNumerical(obvlambdamartNumberOptions, prop);

		
		/*------------------------------------------------------------------
		 *	Check LINEASEARCH properties
		 *------------------------------------------------------------------*/
		/** linesearch.libraries */
		correct = correct && checkAllowedLibraries ( "linesearch", LINESEARCH_ALLOWED_LIBRARIES, prop) ;
		
		/** linesearch.quickrank.train-metric */
		/** linesearch.quickrank.test-metric */
		String linesearch_quickrank_train_metric = prop.getProperty("linesearch.quickrank.train-metric");
		String linesearch_quickrank_test_metric = prop.getProperty("linesearch.quickrank.test-metric");
		if(!ArrayUtils.contains(QUICKRANK_ALLOWED_METRICS, linesearch_quickrank_train_metric)) {
			Execution.logger.fatal(linesearch_quickrank_train_metric + " not accepted value for linesearch.quickrank.train-metric");
			correct=false;
		}
		if(!ArrayUtils.contains(QUICKRANK_ALLOWED_METRICS, linesearch_quickrank_test_metric)) {
			Execution.logger.fatal(linesearch_quickrank_test_metric + " not accepted value for linesearch.quickrank.test-metric");
			correct=false;
		}
		
		correct = correct && checkIsNumerical(linesearchNumberOptions, prop);

		
		/*------------------------------------------------------------------
		 *	Check NORM options
		 *------------------------------------------------------------------*/
		for(String s: normOptions) {
			String temp = prop.getProperty(s);
			if(temp!=null) {
				if(!(temp.equals("sum")||temp.equals("zscore")||temp.equals("linear"))){
					Execution.logger.fatal(temp + " not accepted value for " + s);
					correct = false;
				}
			}
		}

		
		
		/*------------------------------------------------------------------
		 *	Check GRADIENT BOOSTING BINARY CLASSIFIER properties
		 *------------------------------------------------------------------*/
		/** numerical parameters */
		correct = correct && checkIsNumerical(gradientboostingbinaryclassifierNumberOptions, prop);
		return correct;
	}
	

	public static boolean checkAllowedLibraries (String algo, String[] allowed, Properties prop) {
		boolean correct = true;
		
		String algo_libraries = prop.getProperty(algo + ".libraries");
		String[] algo_libraries2= algo_libraries.split(",");
		for(int i = 0; i < algo_libraries2.length; i++) {
			if(ArrayUtils.contains(allowed, algo_libraries2[i])) {
				
			}
			else {
				Execution.logger.fatal(algo_libraries2[i] + " not accepted value for " + algo + ".libraries");
				correct=false;
			}
		}
		return correct;
		
	}
	
	
	public static boolean checkIsNumerical (String[] options, Properties prop) {
		boolean correct = true;
		for(String s: options) {
			String temp = prop.getProperty(s);
			if(temp!=null) { 
				try{
					  Double.parseDouble(temp);
					  // is an number!
				} catch (NumberFormatException e) {
					  // not a number!
					Execution.logger.fatal(temp + " not a number for " + s);
					correct=false;
				}
			}
			else {
				Execution.logger.fatal("Property " + s + " not found!");
			}	
		}
		return correct;
	}
	
}
