package it.unipd.dei.ims.AlgorithmsExecution.parameters.algorithms;

import java.util.Properties;

public interface ReadParameters {
	
	/**
	* Return the String[] of commands to launch for a particular library and a particular algorithm
	* @param prop the properties file.
	* @param algo the algorithm.
	* @param library The library to use.
	* @param runname The name of the run
	* @return an array of String which are the commands for a particular algorithm/library pair
	*/
	public String[] gen_cmdline(Properties prop, String algo, String library, String runname);
	
}
