package it.unipd.dei.ims.AlgorithmsExecution.parameters.algorithms;

import java.io.File;
import java.util.Properties;

import it.unipd.dei.ims.AlgorithmsExecution.Execution;

public class Adarank implements ReadParameters{

	String[] arguments_array = null;
	String arguments = "";
	String runname = "";
	String ranklib_train_metric = "";
	String ranklib_test_metric = "";
	String round = "";
	String tolerance = "";
	String max = "";
	String norm = "";
	String noeq = "";
	
	
	@Override
	public String[] gen_cmdline(Properties prop, String algo, String library,String runname) {
	
				// Get my train, validation and test files...
				String train_file = Execution.out_path + "/info/" + runname + "/" + runname + "_TRAIN" + ".letor";
				String validation_file = Execution.out_path + "/info/"  + runname + "/" + runname + "_VALIDATION" + ".letor";
				String test_file = Execution.out_path + "/info/"  + runname + "/" + runname + "_TEST" + ".letor";
				String score_file = Execution.out_path + "/info/"  + runname +  "/" + runname + "_scores_" + library + ".txt"; 
				
				// Get absolute paths to those files..
				File file_train = new File(train_file);
				String train_path = file_train.getAbsolutePath();
				File file_vali = new File(validation_file);
				String validation_path= file_vali.getAbsolutePath();
				File file_test = new File(test_file);
				String test_path = file_test.getAbsolutePath();
				File file_scores = new File(score_file);
				String scores_path = file_scores.getAbsolutePath();
				
						
				Execution.logger.debug("Class name: " + "MartReadParameters");
				Execution.logger.debug("Algorithm: " + algo);
				Execution.logger.debug("Library: " + library);
				
				if (library.equals("ranklib")) {
					
					arguments_array = new String[2];
					
					arguments = "java -jar bin/" + Execution.ranklib_jar + " -train " + train_path + " -test " + test_path + " -validate " + validation_path +
							" -ranker 3";			
					
					//Parameters specific to Ranklib
					ranklib_train_metric = prop.getProperty("adarank.ranklib.train-metric");
					if(ranklib_train_metric!=null) arguments = arguments + " -metric2t " + ranklib_train_metric;
					ranklib_test_metric = prop.getProperty("adarank.ranklib.test-metric");
					if(ranklib_test_metric!=null) arguments = arguments + " -metric2T " + ranklib_test_metric;
					
					//Parameters commons to every library
					round = prop.getProperty("adarank.round");
					if(round!=null) arguments = arguments + " -round " + round;
					tolerance = prop.getProperty("adarank.tolerance");
					if(tolerance!=null) arguments = arguments + " -tolerance " + tolerance;
					max = prop.getProperty("adarank.max");
					if(max!=null) arguments = arguments + " -max " + max;
					norm = prop.getProperty("adarank.ranklib.norm");
					if(norm!=null) arguments = arguments + " -norm " + norm;
					noeq = prop.getProperty("adarank.noeq");
					if(noeq!=null) arguments = arguments + " -noeq";
					arguments = arguments + " -save mymodel.txt";
					
					
					arguments_array[0] = arguments;
					arguments_array[1] = "java -jar bin/" + Execution.ranklib_jar + " -load mymodel.txt -rank " + test_path + " -score " + scores_path;

					if(Execution.logger.isDebugEnabled()) {
						for (int i = 0; i < arguments_array.length; i++) {
							Execution.logger.debug("Adarank_Ranklib_Command[" + i + "] = " + arguments_array[i] );
						}
					}
				}
				
				else {
					Execution.logger.error("Library name not allowed.");
				}
				
				
				return arguments_array;
	}

}
