package it.unipd.dei.ims.AlgorithmsExecution.parameters.algorithms;

import java.io.File;
import java.util.Properties;

import it.unipd.dei.ims.AlgorithmsExecution.Execution;

public class Linesearch implements ReadParameters {


	String[] arguments_array = null;
	String arguments = "";
	
	String runname = "";
	
	String quickrank_train_metric = "";
	String quickrank_train_cutoff = "";
	String quickrank_test_metric = "";
	String quickrank_test_cutoff = "";
	
	String num_samples = "";
	String windows_size= "";
	String reduction_factor = "";
	String max_iterations = "";
	String max_failed_valid = "";
	String adaptive ="";
	
	String train_partial = "";
	String valid_partial = "";
	
	@Override
	public String[] gen_cmdline(Properties prop, String algo, String library,String runname) {

		// Get my train, validation and test files...
		String train_file = Execution.out_path + "/info/"  + runname + "/" + runname + "_TRAIN" + ".letor";
		String validation_file = Execution.out_path + "/info/"  + runname + "/" + runname + "_VALIDATION" + ".letor";
		String test_file = Execution.out_path + "/info/"  + runname + "/"  + runname + "_TEST" + ".letor";
		String score_file = Execution.out_path + "/info/" + runname + "/"  + runname + "_scores_" + library + ".txt"; 
		
		// Get absolute paths to those files..
		File file_train = new File(train_file);
		String train_path = file_train.getAbsolutePath();
		File file_vali = new File(validation_file);
		String validation_path= file_vali.getAbsolutePath();
		File file_test = new File(test_file);
		String test_path = file_test.getAbsolutePath();
		File file_scores = new File(score_file);
		String scores_path = file_scores.getAbsolutePath();
		
				
		Execution.logger.debug("Class name: " + "LinesearchReadParameters");
		Execution.logger.debug("Algorithm: " + algo);
		Execution.logger.debug("Library: " + library);
		
		if(library.equals("quickrank")) {
			
			arguments_array = new String[3];

			arguments_array[0] = "rm mymodel.xml";
			arguments = "./bin/quicklearn --train " + train_path + " --valid " + validation_path + " --algo LINESEARCH" ;
			
			//Parameters specific to QuickRank
			quickrank_train_metric = prop.getProperty("linesearch.quickrank.train-metric");
			if(quickrank_train_metric!=null) arguments = arguments + " --train-metric " + quickrank_train_metric;
			quickrank_train_cutoff = prop.getProperty("linesearch.quickrank.train-cutoff");
			if(quickrank_train_cutoff!=null) arguments = arguments + " --train-cutoff " + quickrank_train_cutoff;
			quickrank_test_metric = prop.getProperty("linesearch.quickrank.test-metric");
//			if(quickrank_test_metric!=null) arguments = arguments + " --test-metric " + quickrank_test_metric;
			quickrank_test_cutoff = prop.getProperty("linesearch.quickrank.test-cutoff");
//			if(quickrank_test_cutoff!=null) arguments = arguments + " --test-cutoff " + quickrank_test_cutoff;
			
			//Parameters commons to every library
			num_samples = prop.getProperty("linesearch.num-samples");
			if(num_samples!=null) arguments = arguments + " --num-samples " + num_samples;
			windows_size = prop.getProperty("linesearch.windows-size");
			if(windows_size!=null) arguments = arguments + " --windows-size " + windows_size;
			reduction_factor = prop.getProperty("linesearch.reduction-factor");
			if(reduction_factor!=null) arguments = arguments + " --reduction-factor " + reduction_factor;
			max_iterations = prop.getProperty("linesearch.max-iterations");
			if(max_iterations!=null) arguments = arguments + " --max-iterations " + max_iterations;
			max_failed_valid = prop.getProperty("linesearch.max-failed-valid");
			if(max_failed_valid!=null) arguments = arguments + " --max-failed-valid " + max_failed_valid;
			adaptive = prop.getProperty("linesearch.adaptive");
			if(adaptive!=null) arguments = arguments + " --adaptive ";
			
			arguments = arguments + " --model mymodel.xml";
			
			// ./bin/quicklearn --model mymodel.xml --test /.res_TEST.letor --test-metric NDCG --test-cutoff 10 --scores /Users/paolopicello/Documents/workspace/Picello2GUI/resources/scores/quickrank_score_MART.txt

			
			arguments_array[1] = arguments;
			
			arguments_array[2] = "./bin/quicklearn --model mymodel.xml --test " + test_path + " --test-metric " + quickrank_test_metric + " --test-cutoff " + quickrank_test_cutoff + " --scores " + scores_path;
			
			if(Execution.logger.isDebugEnabled()) {
				for (int i = 0; i < arguments_array.length; i++) {
					Execution.logger.debug("Linesearch_QuickRank_Command[" + i + "] = " + arguments_array[i] );
				}
			}
		}
		else {
			Execution.logger.error("Library name not allowed.");
		}
		
		return arguments_array;
		
		
	}

}
