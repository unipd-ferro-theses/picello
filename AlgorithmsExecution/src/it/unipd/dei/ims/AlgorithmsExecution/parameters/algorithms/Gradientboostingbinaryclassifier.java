package it.unipd.dei.ims.AlgorithmsExecution.parameters.algorithms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Properties;

import it.unipd.dei.ims.AlgorithmsExecution.Execution;

public class Gradientboostingbinaryclassifier implements ReadParameters {

	static String[] gradientboostingbinaryclassifierOptions = {"gradientboostingbinaryclassifier.jforests.evaluation-metric", "gradientboostingbinaryclassifier.jforests.trees.num-leaves", "gradientboostingbinaryclassifier.jforests.trees.min-instance-percentage-per-leaf", "gradientboostingbinaryclassifier.jforests.trees.min-instance-per-leaf", "gradientboostingbinaryclassifier.jforests.trees.randomized-splits", "gradientboostingbinaryclassifier.jforests.trees.feature-sampling", "gradientboostingbinaryclassifier.jforests.boosting.num-trees", "gradientboostingbinaryclassifier.jforests.boosting.learning-rate", "gradientboostingbinaryclassifier.jforests.boosting.sub-sampling", "gradientboostingbinaryclassifier.jforests.boosting.early-stopping-tolerance", "gradientboostingbinaryclassifier.jforests.trees.max-leaf-output"};

	String[] arguments_array = null;
	String arguments = "";
	
	String runname = "";
	
	String ranklib_train_metric = "";
	String ranklib_test_metric = "";
	String quickrank_train_metric = "";
	String quickrank_train_cutoff = "";
	String quickrank_test_metric = "";
	String quickrank_test_cutoff = "";
	String jforest_eval_metric = "";
	
	/** Parameters for every library */
	String num_trees = "";
	String num_leaves = "";
	String shrinkage = "";
	
	/** Ranklib e Jforest  only */
	String min_leaf_support = "";
	String estop = "";
	
	/** Ranklib only */
	String norm = "";
	String tc = "";
	
	/** Quickrank only */
	String num_thresholds = "";
	
	/**JForest only */
	String min_instance_percentage_per_leaf = "";
	String min_instance_per_leaf = "";
	String sub_sampling = "";
	String feature_sampling = "";
	String randomized_splis = "";
	String features_to_discard = "";
	String features_to_include = "";
	String imbalance_cost_adjustment = "";
	String early_stoppping_tolerance = "";
	String sigmoid_bins = "";
	String max_dcg_truncation = "";
	String cost_function = "";
	
	
	
	@Override
	public String[] gen_cmdline(Properties prop, String algo, String library,String runname) {

		// Get my train, validation and test files...
		String train_file = Execution.out_path + "/info/"  + runname + "/"+ runname + "_TRAIN" + ".letor";
		String validation_file = Execution.out_path + "/info/"  + runname + "/" + runname + "_VALIDATION" + ".letor";
		String test_file = Execution.out_path + "/info/"  + runname + "/" + runname + "_TEST" + ".letor";
		String score_file = Execution.out_path + "/info/"  + runname + "/"  + runname + "_scores_" + library + ".txt"; 
		
		// Get absolute paths to those files..
		File file_train = new File(train_file);
		String train_path = file_train.getAbsolutePath();
		File file_vali = new File(validation_file);
		String validation_path= file_vali.getAbsolutePath();
		File file_test = new File(test_file);
		String test_path = file_test.getAbsolutePath();
		File file_scores = new File(score_file);
		String scores_path = file_scores.getAbsolutePath();
		
				
		Execution.logger.debug("Class name: " + "GradientBoostingBinaryCassifier");
		Execution.logger.debug("Algorithm: " + algo);
		Execution.logger.debug("Library: " + library);

		if(library.equals("ranklib")) {
			
			
		}
		else if(library.equals("quickrank")) {

		}
		
		else if(library.equals("jforests")) {

			String properties_file = prop.getProperty("jforests.prop_file");

			try {
				createGradientBoostingJForestProperties( prop);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			arguments_array = new String[7];
			arguments_array[0] = "rm f1_train.txt f1_train.bin f1_valid.txt f1_valid.bin f1_test.txt f1_test.bin jforests-discrete-f1_test.txt jforests-discrete-f1_train.txt jforests-feature-stats.txt";
			arguments_array[1] = "cp " + train_path + " f1_train.txt";
			arguments_array[2] = "cp " + test_path + " f1_test.txt";
			arguments_array[3] = "cp " + validation_path + " f1_valid.txt";
			arguments_array[4] = "java -jar " + Execution.jforests_jar + " --cmd=generate-bin --ranking --folder . --file f1_train.txt --file f1_valid.txt --file f1_test.txt";
			arguments_array[5] = "java -jar " + Execution.jforests_jar + " --cmd=train --ranking --config-file " + properties_file +" --train-file f1_train.bin --validation-file f1_valid.bin --output-model ensemble.txt";
			arguments_array[6] = "java -jar " + Execution.jforests_jar + " --cmd=predict --ranking --model-file ensemble.txt --tree-type RegressionTree --test-file f1_test.bin --output-file " + scores_path;
			
		}
		else {
			Execution.logger.error("Library name not allowed.");
		}
		
		return arguments_array;
	}
	
	/**
	* This method writes a properties file in the path specified by the properties of the program.
	* In this way i can read the properties fpr a particular algorithm and generate dinamically
	* the jforest properties to use that particular algorithm
	* @param algo The algorithm used.
	* @param prop The properties file of the program
	*/
	public void createGradientBoostingJForestProperties (Properties prop) throws FileNotFoundException {
		
		String jforestFolder = "";
		jforestFolder=prop.getProperty("jforests.path");

		String properties_file = prop.getProperty("jforests.prop_file");
		File jfor_prop = new File (jforestFolder + "/" + properties_file);
		
//		PrintWriter pw = new PrintWriter(properties_file);
		PrintWriter pw = new PrintWriter(jfor_prop.getAbsolutePath());
	
		
		pw.write("learning.algorithm=");
		pw.write(prop.getProperty("gradientboostingbinaryclassifier.learning.algorithm")+"\n");
		
		
		for(String s: gradientboostingbinaryclassifierOptions) {
//			System.out.println("Parameter: " + s);
			String temp = prop.getProperty(s);

			 s = s.substring("gradientboostingbinaryclassifier.jforests.".length(), s.length());
			if(temp!=null) {
//				System.out.println("Tree Param " + s + " found with value " + temp);
				if(!temp.isEmpty()) pw.write(s+"="+temp+"\n");
			}
		}
		pw.close();		
	
	}

}
