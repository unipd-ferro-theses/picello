package it.unipd.dei.ims.AlgorithmsExecution.parameters.algorithms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Properties;

import it.unipd.dei.ims.AlgorithmsExecution.Execution;

public class Mart implements ReadParameters {

	static String[] gradientboostingOptions = {"mart.jforests.trees.min-instance-percentage-per-leaf", "mart.jforests.trees.randomized-splits", "mart.jforests.trees.feature-sampling", "mart.jforests.boosting.sub-sampling", "mart.jforests.boosting.early-stopping-tolerance", "mart.jforests.trees.max-leaf-output"};

	String[] arguments_array = null;
	String arguments = "";
	
	String runname = "";
	
	String ranklib_train_metric = "";
	String ranklib_test_metric = "";
	String quickrank_train_metric = "";
	String quickrank_train_cutoff = "";
	String quickrank_test_metric = "";
	String quickrank_test_cutoff = "";
	
	String num_trees = "";
	String num_leaves = "";
	String shrinkage = "";
	String min_leaf_support = "";
	String estop = "";
	
	String num_thresholds = "";
	String tc = "";
	String norm = "";
	
	@Override
	public String[] gen_cmdline(Properties prop, String algo, String library, String runname) {

		// Get my train, validation and test files...
		String train_file = Execution.out_path + "/info/"  + runname + "/" + runname + "_TRAIN" + ".letor";
		String validation_file = Execution.out_path + "/info/"  + runname + "/"+ runname + "_VALIDATION" + ".letor";
		String test_file = Execution.out_path + "/info/"  + runname + "/" + runname + "_TEST" + ".letor";
		String score_file = Execution.out_path + "/info/"  + runname + "/"  + runname + "_scores_" + library + ".txt"; 
		
		// Get absolute paths to those files..
		File file_train = new File(train_file);
		String train_path = file_train.getAbsolutePath();
		File file_vali = new File(validation_file);
		String validation_path= file_vali.getAbsolutePath();
		File file_test = new File(test_file);
		String test_path = file_test.getAbsolutePath();
		File file_scores = new File(score_file);
		String scores_path = file_scores.getAbsolutePath();
		
				
		Execution.logger.debug("Class name: " + "MartReadParameters");
		Execution.logger.debug("Algorithm: " + algo);
		Execution.logger.debug("Library: " + library);

		if(library.equals("ranklib")) {
			
			arguments_array = new String[2];
			
			arguments = "java -jar bin/" + Execution.ranklib_jar + " -train " + train_path + " -test " + test_path + " -validate " + validation_path +
					" -ranker 0";			
			
			//Parameters specific to Ranklib
			ranklib_train_metric = prop.getProperty("mart.ranklib.train-metric");
			if(ranklib_train_metric!=null) arguments = arguments + " -metric2t " + ranklib_train_metric;
			ranklib_test_metric = prop.getProperty("mart.ranklib.test-metric");
			if(ranklib_test_metric!=null) arguments = arguments + " -metric2T " + ranklib_test_metric;
			
			//Parameters commons to every library
			num_trees = prop.getProperty("mart.num-trees");
			if(num_trees!=null) arguments = arguments + " -tree " + num_trees;
			num_leaves = prop.getProperty("mart.num-leaves");
			if(num_leaves!=null) arguments = arguments + " -leaf " + num_leaves;
			shrinkage = prop.getProperty("mart.shrinkage");
			if(shrinkage!=null) arguments = arguments + " -shrinkage " + shrinkage;
			min_leaf_support = prop.getProperty("mart.min-leaf-support");
			if(min_leaf_support!=null) arguments = arguments + " -mls " + min_leaf_support;
			estop = prop.getProperty("mart.estop");
			if(estop!=null) arguments = arguments + " -estop " + estop;
			tc = prop.getProperty("mart.ranklib.tc");
			if(tc!=null) arguments = arguments + " -tc " + tc;
			norm = prop.getProperty("mart.ranklib.norm");
			if(norm!=null) arguments = arguments + " -norm " + norm;
			arguments = arguments + " -save mymodel.txt";
			
			arguments_array[0] = arguments;
			arguments_array[1] = "java -jar bin/" + Execution.ranklib_jar + " -load mymodel.txt -rank " + test_path + " -score " + scores_path;

			if(Execution.logger.isDebugEnabled()) {
				for (int i = 0; i < arguments_array.length; i++) {
					Execution.logger.debug("Mart_Ranklib_Command[" + i + "] = " + arguments_array[i] );
				}
			}
			
		}
		else if(library.equals("quickrank")) {
			
			arguments_array = new String[3];

			arguments_array[0] = "rm mymodel.xml";
			arguments = "./bin/quicklearn --train " + train_path + " --valid " + validation_path + " --algo MART" ;
			
			//Parameters specific to QuickRank
			quickrank_train_metric = prop.getProperty("mart.quickrank.train-metric");
			if(quickrank_train_metric!=null) arguments = arguments + " --train-metric " + quickrank_train_metric;
			quickrank_train_cutoff = prop.getProperty("mart.quickrank.train-cutoff");
			if(quickrank_train_cutoff!=null) arguments = arguments + " --train-cutoff " + quickrank_train_cutoff;
			quickrank_test_metric = prop.getProperty("mart.quickrank.test-metric");
//			if(quickrank_test_metric!=null) arguments = arguments + " --test-metric " + quickrank_test_metric;
			quickrank_test_cutoff = prop.getProperty("mart.quickrank.test-cutoff");
//			if(quickrank_test_cutoff!=null) arguments = arguments + " --test-cutoff " + quickrank_test_cutoff;
			
			//Parameters commons to every library
			num_trees = prop.getProperty("mart.num-trees");
			if(num_trees!=null) arguments = arguments + " --num-trees " + num_trees;
			num_leaves = prop.getProperty("mart.num-leaves");
			if(num_leaves!=null) arguments = arguments + " --num-leaves " + num_leaves;
			shrinkage = prop.getProperty("mart.shrinkage");
			if(shrinkage!=null) arguments = arguments + " --shrinkage " + shrinkage;
			min_leaf_support = prop.getProperty("mart.min-leaf-support");
			if(min_leaf_support!=null) arguments = arguments + " --min-leaf-support " + min_leaf_support;
			num_thresholds = prop.getProperty("mart.quickrank.num-thresholds");
			if(num_thresholds!=null) arguments = arguments + " --num_thresholds " + num_thresholds;
			estop = prop.getProperty("mart.estop");
			if(estop!=null) arguments = arguments + " --end-after-rounds " + estop;
			
			arguments = arguments + " --model mymodel.xml";
			
			// ./bin/quicklearn --model mymodel.xml --test /.res_TEST.letor --test-metric NDCG --test-cutoff 10 --scores /Users/paolopicello/Documents/workspace/Picello2GUI/resources/scores/quickrank_score_MART.txt

			
			arguments_array[1] = arguments;
			
			arguments_array[2] = "./bin/quicklearn --model mymodel.xml --test " + test_path + " --test-metric " + quickrank_test_metric + " --test-cutoff " + quickrank_test_cutoff + " --scores " + scores_path;
			
			if(Execution.logger.isDebugEnabled()) {
				for (int i = 0; i < arguments_array.length; i++) {
					Execution.logger.debug("Mart_QuickRank_Command[" + i + "] = " + arguments_array[i] );
				}
			}
		}
		else if(library.equals("jforests")) {
			
			String properties_file = prop.getProperty("jforests.prop_file");

			try {
				createMartJForestProperties( prop);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			arguments_array = new String[7];
			arguments_array[0] = "rm f1_train.txt f1_train.bin f1_valid.txt f1_valid.bin f1_test.txt f1_test.bin jforests-discrete-f1_test.txt jforests-discrete-f1_train.txt jforests-discrete-f1_valid.txt jforests-feature-stats.txt";
			arguments_array[1] = "cp " + train_path + " f1_train.txt";
			arguments_array[2] = "cp " + test_path + " f1_test.txt";
			arguments_array[3] = "cp " + validation_path + " f1_valid.txt";
			arguments_array[4] = "java -jar " + Execution.jforests_jar + " --cmd=generate-bin --ranking --folder . --file f1_train.txt --file f1_valid.txt --file f1_test.txt";
			arguments_array[5] = "java -jar " + Execution.jforests_jar + " --cmd=train --ranking --config-file " + properties_file +" --train-file f1_train.bin --validation-file f1_valid.bin --output-model ensemble.txt";
			arguments_array[6] = "java -jar " + Execution.jforests_jar + " --cmd=predict --ranking --model-file ensemble.txt --tree-type RegressionTree --test-file f1_test.bin --output-file " + scores_path;
			
			
		}
		else {
			Execution.logger.error("Library name not allowed.");
		}
		return arguments_array;
	}

	/**
	* This method writes a properties file in the path specified by the properties of the program.
	* In this way i can read the properties fpr a particular algorithm and generate dinamically
	* the jforests properties to use that particular algorithm
	* @param algo The algorithm used.
	* @param prop The properties file of the program
	*/
	public void createMartJForestProperties (Properties prop) throws FileNotFoundException {

		String jforestFolder = "";
		jforestFolder=prop.getProperty("jforests.path");

		String properties_file = prop.getProperty("jforests.prop_file");
		File jfor_prop = new File (jforestFolder + "/" + properties_file);
		
		PrintWriter pw = new PrintWriter(jfor_prop.getAbsolutePath());
				
		
		String num_trees = prop.getProperty("mart.num-trees");
		pw.write("boosting.num-trees=" + num_trees + "\n");
		String num_leaves = prop.getProperty("mart.num-leaves");
		pw.write("trees.num-leaves=" + num_leaves + "\n");
		String shrinkage = prop.getProperty("mart.shrinkage");
		pw.write("boosting.learning-rate=" + shrinkage + "\n");
		String min_leaf_sup = prop.getProperty("mart.min-leaf-support");
		pw.write("trees.min-instance-per-leaf=" + min_leaf_sup + "\n");
		
		
		for(String s: gradientboostingOptions) {
//			System.out.println("Parameter: " + s);
			String temp = prop.getProperty(s);

			 
			s = s.substring("mart.jforests.".length(), s.length());
			if(temp!=null) {
//				System.out.println("Tree Param " + s + " found with value " + temp);
				if(!temp.isEmpty()) pw.write(s+"="+temp+"\n");
			}
		}
	
		pw.write("learning.algorithm=");
		pw.write(prop.getProperty("mart.jforests.learning.algorithm")+"\n");
		
		pw.write("learning.evaluation-metric=");
		pw.write(prop.getProperty("mart.jforests.evaluation-metric")+"\n");

		pw.close();		

	}

}
