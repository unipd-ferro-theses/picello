package it.unipd.dei.ims.AlgorithmsExecution;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.List;


/** This class define the methods for the implementation
 * 	of the threadPool for parallel computation of 
 * 	Letor files.
 * 
 * @author	Paolo Picello 
 * @email	paolo.picello@studenti.unipd.it
 *  
 */
public class LetorThread implements Runnable {
  
    private String qid;
    private String runs_folder;
    private String runname;
    
    public LetorThread(String qid, String runs_folder, String runname){
        this.qid=qid;
        this.runs_folder= runs_folder;
        this.runname = runname;
    }

    @Override
    public void run() {
        Execution.logger.debug(Thread.currentThread().getName()+" Start. QID = "+qid);
        try {
			computeLetor();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }

	/**
	* Compute Letor file for a particular run and query id.
	* The execution of this method create a file qid.letor in
	* resources/out/runname. These files will then be merged 
	* according to train/validation/test division.
	* @throws InterruptedException if the thread is interrupted.
	* @throws IOException if I/O exceptions occurs.
	*/
    private void computeLetor() throws IOException, InterruptedException {

	   	PrintWriter pw = null;				
		String filename =  runs_folder + "/" + runname;
    	HashMap<String, Long> pointerMemory = new HashMap<String,Long>(); // reset every time we process a new qid 
	    
		/*------------------------------------------------------------------
		 * Compute LETOR
		 *------------------------------------------------------------------*/			    
		pw = new PrintWriter (Execution.out_path + "/info/" + runname + "/letors" + "/" +  qid + ".letor"); // where to print
    	
		long sumStartTime = 0;
		if(Execution.logger.isTraceEnabled()) // we don't want to get time info if we are in not in trace mode
			sumStartTime = System.currentTimeMillis();

		String tableName= Execution.tables_path + "/table" + qid +".txt";
		int qid = Integer.parseInt(this.qid);
	    RandomAccessFile raf = new RandomAccessFile(tableName, "r");
		        
	    /** Load memory pointers for this qid **/
		try {
			pointerMemory = Utility.readFromHashMapFile(qid);
		} catch (ClassNotFoundException e) {
			Execution.logger.fatal("Error loading pointerMemory for qid: " + qid );
			e.printStackTrace();
		}
		BufferedReader run_buffer = new BufferedReader(new FileReader(filename));
		List<String> listDocno = Run.readRunByQueryID(run_buffer, qid); // get the list ofdocno in that file with this qid
			
	    /*------------------------------------------------------------------
		 *  For each docno for that qid (Get letor line from mmemory)
		 *------------------------------------------------------------------*/
		for(int t = 0; t < listDocno.size(); t++) { // for each DOCNO with that queryID			
			boolean not_found = false;		
			long pointer = 0;
			
			/** If not found ignore this docno, this situation will never happens in final scenario
			 * 	This is the case in which we have documents in the runs file for which we have not
			 * 	precomputed letor lines in Picello..
			 */
			if(pointerMemory.containsKey(listDocno.get(t))) // just to verify we have this entry
				pointer = pointerMemory.get(listDocno.get(t));
			else not_found = true;
										
			if(not_found == false) {
				Letor.writeLineLetor(pw, pointer, raf, qid, listDocno.get(t)); // write new Letor line
			}
			
					
		}
		
		if(Execution.logger.isTraceEnabled()) { // we don't want to get time info if we are in not in trace mode
			long SumendTime   = System.currentTimeMillis();
			long SumtotalTimeTOT = SumendTime - sumStartTime;
			double SumtotalTimeTOTs = (double)SumtotalTimeTOT/1000;
			Execution.logger.trace("Time for qid:"+ qid + ": " + SumtotalTimeTOTs); 
		}
		
		Execution.logger.info("Finished to compute LETOR entries for qid:" + qid);
    
	    // empty the hashmap for the next qid to be processed
	    pointerMemory.clear();
	    pw.close();
    }
    

    @Override
    public String toString(){
    	return this.qid;
    }
}