package it.unipd.dei.ims.AlgorithmsExecution;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import it.unipd.dei.ims.AlgorithmsExecution.parameters.algorithms.ReadParameters;
import it.unipd.dei.ims.AlgorithmsExecution.parameters.CheckProperties;

/** This class controls the execution of the program
 * 
 * @author	Paolo Picello 
 * @email	paolo.picello@studenti.unipd.it
 *
 */
public class Execution {
	
	public static String arguments;
	static String algorithms;
	static String runsFolder;
	static String file_properties;
	static String logger_level;
	
	public static String out_path;
	public static String tables_path;
	public static String pointers_path;
	
	public static String ranklib_jar;
	public static String jforests_jar;
	
	public static Logger logger;


	public static void main(String args[]) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		
		long startTime = System.currentTimeMillis();
		
		// Configure logging options (e.g log file for last execution)
		Utility.configureLogger();
	    
		logger = Logger.getLogger("execution_logger");	
		
		boolean multi_thread = true; // this is used to control the multithreading only for merging letor files, computing them uses a ThreadPool and it will work even with less processors
		
		/** Get available processors */
		logger.info("Available processors:" + Runtime.getRuntime().availableProcessors());
		if(Runtime.getRuntime().availableProcessors() < 3){ // controls multithreading only for merging letor files
			logger.warn("Can't use MultiThread version, at least 3 processors need to be available to the JVM for merging letors concurrently.");
			logger.warn("Single Thread version will be used.");
			multi_thread = false;
		}
		
		/** Get properties file from cmd line or set to default */
		if(args.length < 1) { // properties file path not passed as cmd line arguments
			file_properties = "resources/config.properties";
			logger.warn("Path to properties not passed as argument.. The default location resources/config.properties will be used");
		}
		else {
			file_properties = args[0]; // first argument properties file
		}
		
		
		/*------------------------------------------------------------------
		 *  Read general properties from file (terrier_home, train, test, score files etc..)
		 *------------------------------------------------------------------*/
		Properties prop = new Properties();
		InputStream input = null;
		try {			
			input = new FileInputStream(file_properties);
			prop.load(input); 
			
			//----- Define logger level
			logger_level = prop.getProperty("logger_level").toUpperCase();
			Level level = Utility.getLoggerLevel(logger_level);
			logger_level = "Level." + logger_level; // this will be our logger level, defined in the properties file
			logger.setLevel(level);
			//--------------------

			boolean correct = CheckProperties.checkProperties(prop); // Check if properties are well formatted
			logger.debug("Exec_boolean:" + correct);
			
			/** Check if properties file is correctly formed */
			if(correct) { 
				logger.info("-----------------------------" );	
				logger.info("Properties file parsed correctly");
				logger.info("-----------------------------" );	
				
				// Read general properties from properties file
				out_path = prop.getProperty("out_path");
				tables_path = prop.getProperty("tables_path");
				pointers_path = prop.getProperty("pointers_path");
				runsFolder = prop.getProperty("runsFolder");
				ranklib_jar = prop.getProperty("ranklib.jarfile");
				jforests_jar = prop.getProperty("jforests.jarfile");
				String[] runsNames = Utility.listContents(runsFolder);
				if(logger.isDebugEnabled()) { 
					for( int u=0; u<runsNames.length; u++) {
						logger.debug("run name: " + runsNames[u]);
					}
				}
				
				/*------------------------------------------------------------------
				 *  Read features from properties and create Features.features
				 *  ( Which features are already computed and saved on disk )
				 *------------------------------------------------------------------*/
			    Features.readFeatures(); // read features for which in Picello1 we have computed features
			    
			    logger.debug("Num features in the file on disk: " + Features.features.size());

			    Features.getUsedFeatures(prop); // get used features from config.properties

			    logger.debug("Num features actually considered: " + LearningToRank.numFeatures);

			    
			    // Debug: Print features info
				if(logger.isDebugEnabled()) { 
			    	logger.debug("--------------------");
			    	logger.debug("Computed features:");
					for (int i=0; i<Features.features.size(); i++) {
						Features tmp = Features.features.get(i);
						logger.debug("name: " + tmp.name);
						logger.debug("classname: " + tmp.class_name);
						logger.debug("datatype: " + tmp.data_type);
						logger.debug("Valid: " + tmp.valid);
			    		logger.debug("-----------");
					}
			    	logger.debug("--------------------");
			    	logger.debug("Used features:");
				    for(int i =0; i<Features.used_features.size(); i++) {
				    	Features feat = Features.used_features.get(i);
				    	logger.debug("Name: " + feat.name + " class: " + feat.class_name + " valid: " + feat.valid+ " type: " + feat.data_type);
				    }
				}

				/*------------------------------------------------------------------
				 *  For each run in runs_folder
				 *------------------------------------------------------------------*/
				for (String run : runsNames) {
					logger.debug("Runnname:" + run);
					LearningToRank ltr = new LearningToRank(run);
					
					/*------------------------------------------------------------------
					 *  Compute LETOR files for this run (with ThreadPool)
					 *  
					 *  The letor for every qid will be written in a different file by
					 *  different threads to avoid locking mechanism for writing to the
					 *  same file
					 *------------------------------------------------------------------*/
					
					// We create two folder for each run: one will contain the reranked results
					// and the other will contain configuration infos along with letor files used
					File dir_runs = new File(out_path + "/runs"); 
					File dir_info = new File(out_path + "/info/" +run); 
					if(!dir_runs.exists())
						dir_runs.mkdirs(); 
					if(!dir_info.exists())
						dir_info.mkdirs();
					
					String train_qids = prop.getProperty("train"); // get String of train topics (e.g 351,369,370,...)
					String validation_qids = prop.getProperty("validation"); // get String of validation topics
					String test_qids = prop.getProperty("test"); // get String of test topics
					String[] qidsTestArray = test_qids.split(",");
					String tot_qids = train_qids + "," + validation_qids + "," + test_qids; // total String of topics used, useful for ThreadPool
					String[] qidsArray = tot_qids.split(",");
			
				    File letors_folder = new File(out_path + "/info/" + run + "/letors/");
				    
					/*------------------------------------------------------------------
					 *  Computing LETOR
					 *------------------------------------------------------------------*/
				    /** Already extracted letor files.. We only need to merge according to train/validation/test split */
				    if(letors_folder.isDirectory()) {
				    	// WARNING: DANGEROUS SITUATION
				    	// it is useful if we want to rerank run for not already computed algo/library, in such
				    	// situation we don't have to compute letors again
				    	logger.warn("----------------------------");
				    	logger.warn("Already retrieved letor lines from disk (for considered features), we only need to merge according to train/vali/test split.");
				    	logger.warn("ATT: doing this you are faster but you can have problem if the letor are generated with different considered features");
				    	logger.warn("Verify in info/runname/letors/info.txt which features are considered in the respective letors");
				    	logger.warn("Be careful!!!");
				    	logger.warn("----------------------------");

				    }
				    /** Not already extracted letor files, we need to load them from tables and memoryPointers */
				    else { // ..run_name/letors not already computed => extract from table and memoryPointers
						long startTime5 = 0;
						if(Execution.logger.isTraceEnabled()) 
							startTime5 = System.currentTimeMillis();

						letors_folder.mkdirs(); // create directory
				    	
						/** Start Thread Pool */
						ExecutorService pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()); // start a pool with a number of thread equals to the number of processors available to the JVM
						for(String s: qidsArray) { // for each topic
				            Runnable worker = new LetorThread(s, runsFolder, run); 
				            pool.execute(worker); // create Letor file
						}
				        pool.shutdown();
				        while (!pool.isTerminated()) {
				        }

				        // Printing read-only information about the letor generated in this folder
				        Features.printInfoFile(letors_folder);
				        
				        logger.info("Finished all threads");
				        
						if(Execution.logger.isTraceEnabled()) { // we don't want to get time info if we are in not in trace mode
							long endTime5 = System.currentTimeMillis();
							long totalTime5 = endTime5 - startTime5;
							double totalTimeSec5 = (double)totalTime5/1000;
							logger.trace("TOTAL TH_POOL LETOR TIME: " + totalTimeSec5 + " s.");
						}
				    }
	
					
					/*-----------------------------------------------------------------------
					 *  Merging LETOR files created previously for train,validation and test
					 *-----------------------------------------------------------------------*/
					
					String output_train= dir_info + "/" + run + "_TRAIN.letor";
					String output_validation= dir_info + "/" + run + "_VALIDATION.letor";
					String output_test= dir_info +"/" + run + "_TEST.letor";
					
					long startTime6 = 0;
					if(Execution.logger.isTraceEnabled()) // we don't want to get time info if we are in not in trace mode
						startTime6 = System.currentTimeMillis();
					
					// ------ SINGLE-THREAD MERGING
					if(multi_thread == false) {
						Letor.mergeLetor(train_qids,output_train,run);
						Letor.mergeLetor(validation_qids,output_validation,run);
						Letor.mergeLetor(test_qids, output_test,run);
					}
					
					// ------ MULTI-THREAD MERGING
					else {
						logger.info("-----------------------------" );	
						logger.info("Merging LETOR files...");
						/** 
						 * 	Multi-Thread: 1 thread per Task (train,validation,test)
						 * 	Every thread write to a different file to avoid locking in writing the same file
						 */
						Thread t1 = new Thread(new MergeThreads(train_qids,output_train,run));
						Thread t2 = new Thread(new MergeThreads(validation_qids,output_validation,run));
						Thread t3 = new Thread(new MergeThreads(test_qids,output_test,run));
						t1.start();
						t2.start();
						t3.start();
			
						try {
							t1.join();
							t2.join();
							t3.join();
							logger.info("Finished merging LETOR files...");
							logger.info("-----------------------------" );	

							
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					
					if(Execution.logger.isTraceEnabled()) { 
						long endTime6   = System.currentTimeMillis();
						long totalTime6 = endTime6 - startTime6;
						double totalTimeSec6 = (double)totalTime6/1000;
						logger.trace("TOTAL TIME TO MERGE FILES (multiThread="+ multi_thread + ") = " + totalTimeSec6 + " s.");
					}

					//------------------------------------------------------

					/*------------------------------------------------------------------
				 	*  Learning To Rank
				 	*------------------------------------------------------------------*/
					algorithms = prop.getProperty("algorithms");
					algorithms = algorithms.toLowerCase(); // trasform to lower case to avoid problem
					logger.debug("Algorithms used: " + algorithms);
					String[] algos= algorithms.split(",");	
					if(!algorithms.isEmpty()) { // If there are algorithms to be executed 

						File tmp_time = new File("/Users/paolopicello/Desktop/times.txt");
						PrintWriter pw = new PrintWriter(tmp_time);
						
						long startAlgo = 0;
						/*------------------------------------------------------------------
					 	*  Algorithms Execution
					 	*------------------------------------------------------------------*/
						/** For each algorithm i want to use */
						for(int i = 0; i < algos.length; i++) {

							logger.debug("Read configuration for algo: " + algos[i] + ".");
							String libraries2 = prop.getProperty(algos[i] + ".libraries"); // get the libraries we want to use with that algorithm
							String[] libraries = libraries2.split(",");
							
							/** For each library that implement that algorithm that i want to use */
							for(int k = 0; k < libraries.length; k++) {
					
								startAlgo=System.currentTimeMillis();
								
								File output_file = new File(Execution.out_path + "/runs/" + run + "_" + algos[i] + "_" + libraries[k] + ".txt");
								if(output_file.exists()) {
									Execution.logger.warn("File : " + output_file.getAbsolutePath() + " already exists. Skip to prevent unwanted changes");
									Execution.logger.warn("Ignoring rerank for Library: " + libraries[k] + " Algo: " + algos[i]);
								}
								else{
									logger.info("Algorithm " + algos[i] + " executed with " + libraries[k] + "...");

									Class<?> clazz;
									String capClass = algos[i].substring(0, 1).toUpperCase() + algos[i].substring(1); // i want to capitalize only the first letter (because the name of the classes are Mart, Linesearch etc..)
									clazz = Class.forName("it.unipd.dei.ims.AlgorithmsExecution.parameters.algorithms." + capClass);
									Constructor<?> constructor = clazz.getConstructor();
									ReadParameters instance = (ReadParameters) constructor.newInstance();
					
									/** Lunch commands with ProcessBuilder */
									String[] cmd_line;
									cmd_line = instance.gen_cmdline(prop, algos[i], libraries[k], run); // generate commands for this library/algo pair
					
									if(logger.isDebugEnabled()) { 
										for(int j = 0; j < cmd_line.length; j++) {
											logger.debug("Command n." + j + ":" + cmd_line[j]); // commands for this library/algo
										}
									}
								
									/** Execute an algorithm for al ibrary with a given command */
									ltr.librariesExecution (libraries[k], algos[i], cmd_line, output_test, prop, qidsTestArray) ;
								
									long endTime = System.currentTimeMillis();
									long totalTime = endTime - startAlgo;
									double totalTimeSec = (double) totalTime / 1000;
									pw.write("Time for algo: " + algos[i] + " library: " + libraries[k] + " = " + totalTimeSec);
								}
							}
				
							logger.info("End computation for algorithm:" + algos[i]);
							logger.info("--------------------------");

						}
						pw.close();
					}
					else logger.warn("No algorithms specified");
					
					File file1 = new File(output_train);
					File file2 = new File(output_validation);
					File file3 = new File(output_test);
					file1.delete();
					file2.delete();
					file3.delete();
					
					File prop_dst = new File(dir_info + "/" + "config.properties");
					File prop_src = new File(file_properties);
					Utility.copyFileUsingFileChannels(prop_src, prop_dst);
					
					
				}
			}	
			else {
				logger.fatal("Properties file not correctly parsed");
			}
		
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		if(Execution.logger.isTraceEnabled()) { 
			long endTime = System.currentTimeMillis();
			long totalTime = endTime - startTime;
			double totalTimeSec = (double)totalTime/1000;
			logger.trace("TOTAL EXECUTION TIME: " + totalTimeSec + " s.");
		}
		
		logger.info("Execution Completed");
	}
}


