package it.unipd.dei.ims.AlgorithmsExecution;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

/** This class models a single Feature and controls the letor features 
 * 
 * @author	Paolo Picello 
 * @email	paolo.picello@studenti.unipd.it
 *  
 */
public class Features {

	String name = "";
	String class_name = "";
	String data_type = "";
	boolean valid = true; //indicates if this feature should be used, according to  "used_features="
	
	public static List<Features> features = new ArrayList<Features>();
	public static List<Features> used_features = new ArrayList<Features>();
	

	/** A Features object is made up by a name, a data type (Integer or Double), a field index and a boolean indicating if that features should be used or not */
	public Features (String name, String classname, String datatype, boolean valid) {
		this.name = name;
		this.class_name = classname;
		this.data_type = datatype;
		this.valid = valid;
	}
	
	/** Set if a feature will be used in the letor generation or not */
	public void setValid (boolean valid) {
		this.valid = valid;
	}
	

	/** 
	* Read features from file properties.
	* The syntax is as follow: <p>
	* <li> terrier.&ltfeature-name&gt For features computed with Terrier.
	* <li> features.&ltfeature-name&gt For features defined in FeaturesExtraction.
	* <li> features.&ltfeature-name&gt-&ltField-name&gt For features defined in FeaturesExtraction computed on a single field.
	* @param prop The properties file.
	 * @throws IOException 
	*/
	public static void readFeatures () throws IOException {
		
		// load properties file from tables folder 
		Properties prop = new Properties();
		InputStream input = new FileInputStream(Execution.tables_path + "/features.properties");
		prop.load(input); // load a properties file
		String featurestext = prop.getProperty("features");
		
		// read features as written on disk.. after this I will read which we use
		int feat_cnt = 0;
		Scanner scanner = new Scanner(featurestext);
		while(scanner.hasNext()) {
			scanner.next();
			feat_cnt++;
		}
		scanner.close();
		String[] featuresNames = new String[feat_cnt];
		scanner = new Scanner(featurestext);
		feat_cnt = 0;
		while(scanner.hasNext()) {
			String feature = scanner.next();
			featuresNames[feat_cnt] = feature;
			feat_cnt++;
		}
		scanner.close();
				
		boolean valid = false; // initially evey feature is not considered
		
		/** for each features in properties */
		for(int i = 0; i < featuresNames.length; i++) {
			String name = "";
			String classname = "";
			String datatype = "";
						
//			// CASE 1: feature contains "-" character
			if(featuresNames[i].contains("-")) { // PER FIELD (found "-".. from there there must be dfiled name othrerwise it is an error.. we are not controlling it here, we are asusming correctness from FeaturesExtraction (we don't see terrier.properties from here) 
				int pos = featuresNames[i].indexOf("-"); // position in which there is -
				String feature_name = featuresNames[i].substring(0, pos);
				
				if(prop.containsKey(feature_name + ".class")) {
					name = featuresNames[i];
					classname = prop.getProperty(feature_name + ".class");
					datatype = prop.getProperty(feature_name + ".datatype");
				}
				else {
					Execution.logger.fatal("Feature " + classname + " not defined.. verify correctness in FeaturesExtraction");
				}
			}
			
			else if(featuresNames[i].startsWith("terrier.")) { // TERRIER FEATURES
				name = featuresNames[i];
				classname = featuresNames[i];
				datatype = "java.lang.Double";
			}
			
			// CASE 2: feature on the whole document
			else {
				name = featuresNames[i];
				classname = prop.getProperty(featuresNames[i] + ".class");
				datatype = prop.getProperty(featuresNames[i] + ".datatype");
			}
		
			Features newFeature = new Features(name, classname, datatype, valid);
			features.add(newFeature);
		}
	}

	public static void getUsedFeatures(Properties prop) {

		String featurestext = prop.getProperty("used_features");
		// Read features from properties
		int feat_cnt = 0;
		
		if(featurestext.equals("all")) {
			LearningToRank.numFeatures = features.size();
			for(int j = 0; j<features.size(); j++) {
				features.get(j).setValid(true);
				used_features.add(features.get(j));
			}
		}
		else {
			Scanner scanner = new Scanner(featurestext);
			while(scanner.hasNext()) {
				scanner.next();
				feat_cnt++;
			}
			scanner.close();
			String[] featuresNames = new String[feat_cnt];
			scanner = new Scanner(featurestext);
			feat_cnt = 0;
			while(scanner.hasNext()) {
				String feature = scanner.next();
				featuresNames[feat_cnt] = feature;
				feat_cnt++;
			}
			scanner.close();
		
			int count_active = 0;
		
			// Get if this feature is used
			for(int i = 0; i< featuresNames.length; i++) { // for each used feature
				boolean found = false;
				for(int j = 0; j<features.size(); j++) { // for each feature computed (these are from resources/tables)
					if(featuresNames[i].equals(features.get(j).name)) {
						features.get(j).setValid(true);
						found = true;
						count_active++;
						used_features.add(features.get(j));
					}
				}
			
				if (found==false) {
					Execution.logger.fatal("Feature " + featuresNames[i] + " not defined.. Control in feature.properties in \"tables_path\" is this feature is defined");
					System.exit(0);
				}
			
			}
			LearningToRank.numFeatures = count_active;
		}
	}

	public static void printInfoFile(File file) throws FileNotFoundException {
		
		PrintWriter pw = new PrintWriter(file + "/info.txt");
		
		pw.write("#This file is a READ/ONLY file.. contains informations on the features contained"
				+ " in the letor files in this folder \n");
		
		for(int i=0; i<Features.used_features.size(); i++) {
			Features feat = Features.used_features.get(i);
			pw.write(i+1 + ":" + feat.name + "\n");
		}
		
		File read_only = new File (file + "/info.txt");
		read_only.setReadOnly();
		
		pw.close();

		
	}
}
