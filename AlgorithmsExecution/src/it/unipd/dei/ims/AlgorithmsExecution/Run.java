package it.unipd.dei.ims.AlgorithmsExecution;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/** This class control the model of a Run entry
 * 
 * @author	Paolo Picello 
 * @email	paolo.picello@studenti.unipd.it
 *  
 */
public class Run {
	
	int queryID;
	String docNO;	
	double score;
	
	static HashMap<Integer, List<Run>> hashMap = new HashMap<Integer, List<Run>>(); 

	static List<Run> runs = new ArrayList<Run>();
	

	public Run(int queryID, String docNO, double score) {
		this.queryID = queryID;
		this.docNO = docNO;
		this.score = score;
	}
	
	/**
	* Print queryId, DocId, Model and score for a particular run entry 
	* @param run to be printed.
	*/
	public static void printRun(Run run, int index) {
		Execution.logger.debug("" + run.queryID + " Q0");
		Execution.logger.debug(" " + run.docNO);
		Execution.logger.debug(" " + index + " " + run.score + " SCORE");
	}
	
	public static void printRunFile(Run run, int index, PrintWriter pw, String runName) {
		pw.write("" + run.queryID + " Q0");
		pw.write(" " + run.docNO);
		pw.write(" " + index + " " + run.score + " " + runName);
		pw.write("\r\n");
	}
	
	/**
	* Populate hashMap for a particular qid. In Run.hashmap there will be couples in the form (qid,<lista di run con quel qid>)
	* @param qid the qid
	*/
	public static void populateHashMap(int qid) {
		List<Run> temp_runs = new ArrayList<Run>();

		for(int i = 0; i < Run.runs.size(); i++) {
			Run run = Run.runs.get(i);
			if(run.queryID == qid) {
				temp_runs.add(run);
			}
		}
		hashMap.put(qid, temp_runs);

	}

	/**
	* This returns a list of docno for a particular qid in a particular file.
	* We can do better, this way we are searching for all even if we know that they are ordered and once i found the first when i found one
	* different we can stop. But doing this we will no longer accept not ordered files. This operation is very fast and not represent a 
	* bottleneck in the execution of the program so this can be avoided.
	* @param in BufferedReader for the file.
	* @param queryID the id of the query.
	* @throws IOException if cannot read the runs.
	*/
	public static List<String> readRunByQueryID (BufferedReader in, int queryID) throws IOException {
		List<String> docnoList = new ArrayList<String>();
		String line = in.readLine();
			
		String[] splitStr = line.split("\\s+");
		int actualQueryID = Integer.parseInt(splitStr[0]);

		while(line != null) {
			splitStr = line.split("\\s+");
			actualQueryID = Integer.parseInt(splitStr[0]);
			
			if(actualQueryID == queryID ) {
				docnoList.add(splitStr[2]);
			}
			line=in.readLine();
		}
		return docnoList;

	}
	
	public int getQueryID(){
		return queryID;
	}
	
	public String getDocNo(){
		return docNO;
	}
	
	public double getValue() {
		return score;
	}

}

/**
* A comparator to determine the order of the list
*/
class RunComparatorByScoring implements Comparator<Run> {
    @Override
    public int compare(Run o1, Run o2) {
        return Double.compare(o2.getValue(), o1.getValue());
    }
}


