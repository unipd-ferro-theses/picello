package it.unipd.dei.ims.AlgorithmsExecution;

import java.io.IOException;

/** This class is used to create a Thread for merging previously
 * 	generated letor files according to train/validation/test division.
 * 
 * @author	Paolo Picello 
 * @email	paolo.picello@studenti.unipd.it
 * 
 */
public class MergeThreads implements Runnable {

		   private Thread t;
		   private String qids;
		   private String output;
		   private String run;
		   
			/**
			* Read features from file properties.
			* @param qids The string representing the qids for this task.
			* @param output Name (path) of the output file.
			* @param run Name of the run.
			*/
		   public MergeThreads(String qids, String output, String run) {
		      this.qids = qids;
		      this.output = output;
		      this.run = run;
		   }
		   

		public void run() {
	
			   try {
				Letor.mergeLetor(qids,output,run);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		   }
		   
		   public void start () {
		      if (t == null) {
		         t = new Thread ();
		         t.start ();
		      }
		   }
		

}